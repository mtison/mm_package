#include <ros/ros.h>
#include <stdio.h>
#include "data_sglton.h"

#include <gtsam/geometry/Rot3.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/navigation/GPSFactor.h>
#include <gtsam/navigation/ImuFactor.h>
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/ISAM2.h>

#include <nodelet/nodelet.h>
#include <std_msgs/String.h>
#include <pluginlib/class_list_macros.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>

#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <std_msgs/Float32.h>


#include <eigen3/unsupported/Eigen/CXX11/Tensor>
#include <eigen3/unsupported/Eigen/MatrixFunctions>



namespace nodelet_ns
{

    class Localizer_nlet : public nodelet::Nodelet
    {
        public :
            Localizer_nlet(void);

        private :
            Data_sglton* data;
            ros::Subscriber subGPS;
            ros::Subscriber subImu;
            ros::Subscriber subOptDone;
            ros::Publisher pubLocalization;
        	Eigen::VectorXd xnk;
        	Eigen::VectorXd xnk_1;
        	Eigen::VectorXd dxk;
        	Eigen::VectorXd dxk_1;
        	Eigen::VectorXd xk;
        	Eigen::VectorXd xk_1;
        	Eigen::VectorXd uk;
        	Eigen::VectorXd uk_1;
        	Eigen::MatrixXd covdxk;
        	Eigen::MatrixXd covdxk_1;
        	std::deque<sensor_msgs::Imu> localimuQueue4projecting;

        	double k;
        	double lastgpstime;

            virtual void onInit(void);
            void gpsHandler(const nav_msgs::Odometry::ConstPtr& gpsMsg);
            void optdoneHandler(const std_msgs::Float32::ConstPtr& dummy);
            void imuHandler(const sensor_msgs::Imu::ConstPtr& imu_raw);
            void MEKF_time_update(sensor_msgs::Imu);
            void onlymean_time_update(sensor_msgs::Imu);
            void MEKF_gpspos_measurement_update(nav_msgs::Odometry);
            void MEKF_gpsyaw_measurement_update(double);
            void MEKF_ahrsrollpitch_measurement_update(sensor_msgs::Imu );
            Eigen::VectorXd f_nominal(Eigen::VectorXd, Eigen::VectorXd);
            std::vector<Eigen::MatrixXd> f_delta(Eigen::VectorXd, Eigen::VectorXd);
            Eigen::Vector3d gpos(Eigen::VectorXd, Eigen::VectorXd);
            double gyaw(Eigen::VectorXd, Eigen::VectorXd);
            Eigen::Vector2d grollpitch(Eigen::VectorXd, Eigen::VectorXd);
            Eigen::Vector3d ggravity(Eigen::VectorXd, Eigen::VectorXd);
            std::vector<Eigen::MatrixXd> gpos_linearization_discretization(Eigen::VectorXd, Eigen::VectorXd, Eigen::MatrixXd);
            std::vector<Eigen::MatrixXd> gyaw_linearization_discretization(Eigen::VectorXd, Eigen::VectorXd, Eigen::MatrixXd);
            std::vector<Eigen::MatrixXd> grollpitch_linearization_discretization(Eigen::VectorXd, Eigen::VectorXd, Eigen::MatrixXd);
            std::vector<Eigen::MatrixXd> ggravity_linearization_discretization(Eigen::VectorXd, Eigen::VectorXd);


    };

}
                                                                                                                                                                                                          
              
