#include <ros/ros.h>
#include <stdio.h>
#include "data_sglton.h"

#include <gtsam/geometry/Rot3.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/navigation/GPSFactor.h>
#include <gtsam/navigation/ImuFactor.h>
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/ISAM2.h>
#include <gtsam/slam/dataset.h>

#include <nodelet/nodelet.h>
#include <std_msgs/String.h>
#include <pluginlib/class_list_macros.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/icp.h>


#include <nav_msgs/Odometry.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>
#include <pcl/visualization/cloud_viewer.h>
 #include <pcl/io/pcd_io.h>


#include "nanoflann.hpp"
#include "KDTreeVectorOfVectorsAdaptor.h"



namespace nodelet_ns
{

    class Loopcloser_nlet : public nodelet::Nodelet
    {
        public :
            Loopcloser_nlet(void);

        private :
            Data_sglton* data;
            ros::Timer timer;
            node local_last_node;
            std::vector<std::vector<float>> polarcontext_invkeys_to_search_;
            std::unique_ptr<KDTreeVectorOfVectorsAdaptor< std::vector<std::vector<float>>, float>> polarcontext_tree_;
            pcl::VoxelGrid<pcl::PointXYZI> downSizeFilterICP;


            virtual void onInit(void);
            void scancontextmatcher(const ros::TimerEvent&);
            std::pair<int, float> matchScanContextFeatures ( void );
            std::pair<double, int> distanceBtnScanContext( Eigen::MatrixXd &, Eigen::MatrixXd &);
            int fastAlignUsingVkey( Eigen::MatrixXd &, Eigen::MatrixXd &);
            Eigen::MatrixXd circshift( Eigen::MatrixXd &, int);
            double distDirectSC ( Eigen::MatrixXd &, Eigen::MatrixXd &);
            Eigen::MatrixXd createSectorkeyfeature( Eigen::MatrixXd &);
            void loopFindNearKeyframesWithRespectTo(pcl::PointCloud<pcl::PointXYZI>::Ptr&, const int&, const int&);

    };

}

