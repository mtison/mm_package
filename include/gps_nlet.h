#include <ros/ros.h>
#include <stdio.h>
#include "data_sglton.h"

#include <gtsam/geometry/Rot3.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/navigation/GPSFactor.h>
#include <gtsam/navigation/ImuFactor.h>
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/ISAM2.h>

#include <nodelet/nodelet.h>
#include <std_msgs/String.h>
#include <pluginlib/class_list_macros.h>

#include <nav_msgs/Odometry.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>


#include <iostream>
#include <fstream>


namespace nodelet_ns
{

    class Gps_nlet : public nodelet::Nodelet
    {
        public :
            Gps_nlet(void);

        private :
            Data_sglton* data;
            ros::Subscriber subGPS;
            ros::Subscriber subGPSfix;
            ros::Publisher pubGPS;

            bool Tw_utm_definedflag;
            Eigen::Matrix4d T_utm_w;
            Eigen::Matrix4d T_utm_r;
            Eigen::Matrix4d T_w_r;

            std::deque<double> headingvec;

            virtual void onInit(void);
            void gpsfixHandler(const sensor_msgs::NavSatFix::ConstPtr& msg);
            Eigen::VectorXd coef(double, int);
    };

}
