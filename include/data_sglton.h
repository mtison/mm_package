#include <iostream>
#include <stdint.h>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <sensor_msgs/Imu.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>

#include <gtsam/geometry/Rot3.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/navigation/GPSFactor.h>
#include <gtsam/navigation/ImuFactor.h>
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/ISAM2.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

// Returns double only, used for indexes of different part of the state vector
// P(0) = 8718968878589280256
// P(1) = 8718968878589280257
// V(0) = 8502796096475496448
// V(1) = 8502796096475496449
// B(0) = 7061644215716937728
// B(1) = 7061644215716937729
using gtsam::symbol_shorthand::P; // Pose3 (x,y,z,r,p,y)
using gtsam::symbol_shorthand::V; // Vel   (xdot,ydot,zdot)
using gtsam::symbol_shorthand::B; // Bias  (ax,ay,az,gx,gy,gz)

typedef struct Smconstraint
{
	gtsam::Pose3 relT;
	gtsam::noiseModel::Diagonal::shared_ptr relnoise;
	pcl::PointCloud<pcl::PointXYZI>::Ptr cornerptcl;
	pcl::PointCloud<pcl::PointXYZI>::Ptr surfptcl;
	double time;
	int id;
	int previd;
    Eigen::MatrixXd sc;
    std::vector<float> ringkey;
} smconstraint;

typedef struct Ptclconstraint
{
	pcl::PointCloud<pcl::PointXYZI>::Ptr cornerptcl;
	pcl::PointCloud<pcl::PointXYZI>::Ptr surfptcl;
	double time;
	int id;
    Eigen::MatrixXd sc;
    std::vector<float> ringkey;
} ptclconstraint;

typedef struct Node
{
	Eigen::Matrix4d Tw_r;
	Eigen::VectorXd state;
	pcl::PointCloud<pcl::PointXYZI>::Ptr cornerptcl;
	pcl::PointCloud<pcl::PointXYZI>::Ptr surfptcl;
	double time;
	int id;
	bool has_ptcl;
    Eigen::MatrixXd sc;
    std::vector<float> ringkey;
} node;

typedef struct StatenTime
{
	std_msgs::Header header;
	Eigen::VectorXd state;
} statentime;

class Data_sglton
{
public:
    boost::shared_mutex gtsam_mutex;
    boost::shared_mutex node_mutex;
    bool opt_flag;
    bool opt_flag2;
    bool sync_flag;

    boost::shared_mutex optimize_barrier;
    boost::shared_mutex loopcloser_barrier;
    boost::shared_mutex initsynchronize_barrier;

    std::deque<nav_msgs::Odometry> gpsconstraintQueue; // GPS data queue
    std::deque<smconstraint> smconstraintQueue;
    std::deque<ptclconstraint> ptclconstraintQueue;
    std::deque<statentime> localizerQueue4sync;
    std::deque<statentime> localizerQueue4sm;
    std::deque<sensor_msgs::Imu> imuQueue4imupreint;


    gtsam::NonlinearFactorGraph gtSAMgraph; // called 5x. Temporarly hold newest constraints up until they are added to the isam variable
    gtsam::Values initialEstimate; // called 4x. Temporarly hold newest node initial estimates up until they are added to the isam variable
    gtsam::ISAM2 *isam; // called 4x.  THE graph(pose path, full covariance, NOT THE KEYFRAMES) and the initial estimates of each node
    Eigen::MatrixXd lastCovariance; // called 1x. Covariance of the only the current pose
    gtsam::Values graph_opt; // Most uptodate graph optimized by gtsam. NOT of size n_node, but of size n_node_opt
    pcl::PointCloud<pcl::PointXYZI>::Ptr localMapCorner;
    pcl::PointCloud<pcl::PointXYZI>::Ptr localMapSurf;
    int n_node;
    int n_node_opt;
    int localMapN;
    std::deque<node> localMapNodeVec;

    Eigen::Matrix3d accPSD;
    Eigen::Matrix3d gyrPSD;
    Eigen::Matrix3d integrationPSD;
    Eigen::MatrixXd biasPSD;
    Eigen::MatrixXd Q; // PSD matrix of acc,gyr,accbias, gyrbias
    Eigen::MatrixXd initPoseCov;
    Eigen::MatrixXd initVelCov;
    Eigen::MatrixXd initBiasCov;
    Eigen::MatrixXd initCov;

    double sm_cov;
    double sync_rate;
    double imu_rate;

	Eigen::Matrix4d povTlidar_imu;
	Eigen::Matrix4d povTlidar_ahrs;
	int nrow_scan;
	int ncol_scan;

    std::vector<node*> nodes;
    std::vector<node*> nodes_wptcl;

    nav_msgs::Path globalPath;


    int Nr; // Number of rings in the SC matrix ; 20 in the original paper (IROS 18)
    int Ns; // Number of sectors in the SC matrix ; 60 in the original paper (IROS 18)
    double Lmax; // 80 meter max in the original paper (IROS 18)
    int first_stage_ncandidates; // 10 is enough. (refer the IROS 18 paper)



    //Methods
    static Data_sglton *get();
    Eigen::Matrix4d POVtranslaEul2HomoMat(double, double, double, double, double, double);
    void POVhomoMat2TranslaEul(Eigen::Matrix4d, double *, double *, double *, double *, double *, double *);
    void POVhomoMat2TranslaQuat(Eigen::Matrix4d, double *, double *, double *, double *, double *, double *, double *);
    Eigen::Matrix4d POVTranslaQuat2homoMat(double, double, double, double, double, double, double);
    void ZYXeul2Quat(double, double, double, double *, double *, double *, double *);
    void Quat2ZYXeul(double, double, double, double, double *, double *, double *);

    
private:
    //Members
    static Data_sglton *s_Instance;

    //Methods
    Data_sglton();
};
