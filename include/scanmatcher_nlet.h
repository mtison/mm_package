#include "data_sglton.h"

#include <gtsam/geometry/Rot3.h>
#include <gtsam/geometry/Pose3.h>

#include <nodelet/nodelet.h>
#include <std_msgs/String.h>
#include <pluginlib/class_list_macros.h>
#include <sensor_msgs/Imu.h>
#include <ros/ros.h>

#include <std_msgs/Header.h>
#include <std_msgs/Float64MultiArray.h>
#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>

#include <opencv/cv.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/search/impl/search.hpp>
#include <pcl/range_image/range_image.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/icp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/crop_box.h>
#include <pcl_conversions/pcl_conversions.h>

#include <tf/LinearMath/Quaternion.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf_conversions/tf_eigen.h>

// THIS IS ADDED BECAUSE WE ARE USING ROS MELODIC.
// NO NEED TO DO THIS IN ROS NOETIC, ALREADY IN PCL point_types.h
// see last example https://pcl.readthedocs.io/projects/tutorials/en/latest/adding_custom_ptype.html#adding-custom-ptype
struct PointXYZIRT
{
	PCL_ADD_POINT4D
	PCL_ADD_INTENSITY;
	uint16_t ring;
	float time;
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
} EIGEN_ALIGN16;

POINT_CLOUD_REGISTER_POINT_STRUCT (PointXYZIRT,
								  (float, x, x)
								  (float, y, y)
								  (float, z, z)
								  (float, intensity, intensity)
								  (uint16_t, ring, ring)
								  (float, time, time))


namespace nodelet_ns
{

	struct smoothness_t{
		float value;
		size_t ind;
	};

	struct by_value{
		bool operator()(smoothness_t const &left, smoothness_t const &right) {
			return left.value < right.value;
		}
	};



    class Scanmatcher_nlet : public nodelet::Nodelet
    {
        public :
    		Scanmatcher_nlet(void);


        private :

            /////////// Data singleton ///////////
            Data_sglton* data;
            ////////////////////////////

    	    ros::Subscriber subLaserCloud;
    	    ros::Publisher pubExtractedCloud;
    	    ros::Publisher pubCornerPoints;
    	    ros::Publisher pubSurfacePoints;


    	    sensor_msgs::PointCloud2 ptcl_ros;
    	    pcl::PointCloud<PointXYZIRT>::Ptr ptcl_pcl;
    	    pcl::PointCloud<pcl::PointXYZI>::Ptr   extractedCloud;
    	    pcl::PointCloud<pcl::PointXYZI>::Ptr extractedCloudDS;
    	    pcl::VoxelGrid<pcl::PointXYZI> downSizeFilterSC;
    	    pcl::VoxelGrid<pcl::PointXYZI> downSizeFilterFeature;
            std::deque<node> local_map_nodevec;
            pcl::PointCloud<pcl::PointXYZI>::Ptr local_lastcorner_transformed;
            pcl::PointCloud<pcl::PointXYZI>::Ptr local_lastsurf_transformed;
            int local_n_node;
            int local_n_node_opt;
            pcl::PointCloud<pcl::PointXYZI>::Ptr cornerCloud; // corner feature set from odoOptimization
            pcl::PointCloud<pcl::PointXYZI>::Ptr surfaceCloud; // surf feature set from odoOptimization
            pcl::PointCloud<pcl::PointXYZI>::Ptr cornerCloudDS; // downsampled corner featuer set from odoOptimization
            pcl::PointCloud<pcl::PointXYZI>::Ptr surfaceCloudDS; // downsampled surf featuer set from odoOptimization
            pcl::PointCloud<pcl::PointXYZI>::Ptr local_lastcorner_transformedDS;
            pcl::PointCloud<pcl::PointXYZI>::Ptr local_lastsurf_transformedDS;
            pcl::VoxelGrid<pcl::PointXYZI> downSizeFilter;
            Eigen::Matrix4d povSM_Tw_r;
            Eigen::Matrix4d povSM_Trprev_r;
            Eigen::Matrix4d povImuPre_Tw_r;

            Eigen::MatrixXd sc_local;
            std::vector<float> ringkey_local;


    	    std::vector<double> pointRangeVec; // point ranges from range image
    	    std::vector<int> pointColIdxVec;  // point column index from range image
    	    std::vector<int> startRingIdxVec;
    	    std::vector<int> endRingIdxVec;
    	    cv::Mat rangeImage;
    	    std::vector<smoothness_t> cloudSmoothness;
    	    float *cloudCurvature;
    	    int *cloudNeighborPicked;
    	    int *cloudLabel;
            pcl::PointCloud<pcl::PointXYZI>::Ptr laserCloudOri;
            pcl::PointCloud<pcl::PointXYZI>::Ptr coeffSel;
            std::vector<pcl::PointXYZI> laserCloudOriCornerVec; // corner point holder for parallel computation
            std::vector<pcl::PointXYZI> coeffSelCornerVec;
            std::vector<bool> laserCloudOriCornerFlag;
            std::vector<pcl::PointXYZI> laserCloudOriSurfVec; // surf point holder for parallel computation
            std::vector<pcl::PointXYZI> coeffSelSurfVec;
            std::vector<bool> laserCloudOriSurfFlag;
            pcl::KdTreeFLANN<pcl::PointXYZI>::Ptr kdtreeCornerFromMap;
            pcl::KdTreeFLANN<pcl::PointXYZI>::Ptr kdtreeSurfFromMap;
            double transformTobeMapped[6];
            bool isDegenerate;
            cv::Mat matP;
            Eigen::Affine3f transPointAssociateToMap;

            virtual void onInit(void);
    	    void cloudHandler(const sensor_msgs::PointCloud2ConstPtr&);

            Eigen::MatrixXd createScancontextfeature( pcl::PointCloud<pcl::PointXYZI> &);
            std::vector<float> createRingkeyfeature( Eigen::MatrixXd &);

    	    void extractFeatures(void);
    	    void projectandExtractPointCloud(void);
            void scan2MapOptimization(void);
            void cornerOptimization(void);
            void surfOptimization(void);
            void combineOptimizationCoeffs(void);
            bool LMOptimization(int);

    };

}
