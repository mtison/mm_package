#include <ros/ros.h>
#include <stdio.h>
#include "data_sglton.h"

#include <gtsam/geometry/Rot3.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/navigation/GPSFactor.h>
#include <gtsam/navigation/ImuFactor.h>
#include <gtsam/navigation/AHRSFactor.h>
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/ISAM2.h>


#include <gtsam_unstable/nonlinear/IncrementalFixedLagSmoother.h>

#include <nodelet/nodelet.h>
#include <std_msgs/String.h>
#include <pluginlib/class_list_macros.h>
#include <sensor_msgs/Imu.h>


#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/search/impl/search.hpp>
#include <pcl/range_image/range_image.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/icp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/crop_box.h>
#include <pcl_conversions/pcl_conversions.h>

#include <tf/LinearMath/Quaternion.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>

#include <nav_msgs/Odometry.h>

enum Match {Match1, Match2, Match3, Match4, Match5, Match6 };


namespace nodelet_ns
{

    class Synchronizer_nlet : public nodelet::Nodelet
    {
        public :
            Synchronizer_nlet(void);

        private :
            Data_sglton* data;
            ros::Timer timer;
            std::deque<smconstraint> local_smconstraintQueue;
            std::deque<ptclconstraint> local_ptclconstraintQueue;
            std::deque<nav_msgs::Odometry> local_gpsconstraintQueue; // GPS data queue


		    boost::shared_ptr<gtsam::PreintegrationParams> p;

		    gtsam::PreintegratedImuMeasurements *imuIntegratorOpt;

		    Match lastmatch;

            virtual void onInit(void);
            void synchronize(const ros::TimerEvent&);
            void synchronize2(const ros::TimerEvent&);
            void match(void);
            void match1(void);
            void match2(void);
            void match3(void);
            void match4(void);
            void match5(void);
            void match6(void);
            void updatep6Dglobalmap(smconstraint, Eigen::VectorXd, gtsam::Vector3, gtsam::imuBias::ConstantBias);
            void imuHandler(const sensor_msgs::Imu::ConstPtr&);
            void fill_imupre(double);


    };
}
