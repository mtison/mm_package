#include <localizer_nlet.h>

namespace nodelet_ns
{
    Localizer_nlet::Localizer_nlet(void)
    {
    }

    void Localizer_nlet::onInit(void)
    {
        ros::NodeHandle& n = getNodeHandle();

        data = Data_sglton::get();

        subGPS   = n.subscribe<nav_msgs::Odometry> ("gps_localz", 200, &Localizer_nlet::gpsHandler, this, ros::TransportHints().tcpNoDelay());
        subImu = n.subscribe<sensor_msgs::Imu>  ("imu_raw", 2000, &Localizer_nlet::imuHandler,      this, ros::TransportHints().tcpNoDelay());
        subOptDone = n.subscribe<std_msgs::Float32>  ("optimization_done", 2000, &Localizer_nlet::optdoneHandler,  this, ros::TransportHints().tcpNoDelay());
        pubLocalization = n.advertise<geometry_msgs::PoseWithCovarianceStamped> ("localization", 2000);


        k = 0;
        lastgpstime = 0;


        ROS_INFO("\033[1;32m----> localizer_nlet Started.\033[0m");

    }

    // TOPIC /odometry/gps callback
    void Localizer_nlet::gpsHandler(const nav_msgs::Odometry::ConstPtr& gpsMsg)
    {


    	nav_msgs::Odometry gps_raw = *gpsMsg;

		double dummy1, dummy2, yaw;
		data->Quat2ZYXeul(gps_raw.pose.pose.orientation.w, gps_raw.pose.pose.orientation.x, gps_raw.pose.pose.orientation.y, gps_raw.pose.pose.orientation.z, &dummy1, &dummy2, &yaw);

    	MEKF_gpspos_measurement_update(gps_raw);

    	MEKF_gpsyaw_measurement_update(yaw);

    	lastgpstime = ros::Time::now().toSec();

        data->gpsconstraintQueue.push_back(gps_raw);


    }


    void Localizer_nlet::optdoneHandler(const std_msgs::Float32::ConstPtr& dummy)
    {


    	if(ros::Time::now().toSec() - lastgpstime > 1)
    	// Then the gps is in outage
    	{
    		node local_lastNodeOpt = *data->nodes[data->n_node_opt-1];

    		sensor_msgs::Imu last;

    		// First, remove data in imu and gps queue up to the last optimized node
    		while(localimuQueue4projecting.front().header.stamp.toSec() < local_lastNodeOpt.time && !localimuQueue4projecting.empty())
    		{
    	        last = localimuQueue4projecting.front(); // keep to obtain uk_1
    	        localimuQueue4projecting.pop_front();
    		}

			data->localizerQueue4sync.clear();
			xk = local_lastNodeOpt.state;
			xnk = xk;
//			covdxk = data->lastCovariance;
//			uk(0) = last.linear_acceleration.x;
//			uk(1) = last.linear_acceleration.y;
//			uk(2) = last.linear_acceleration.z;
//			uk(3) = last.angular_velocity.x;
//			uk(4) = last.angular_velocity.y;
//			uk(5) = last.angular_velocity.z;

			for(int i=0; i<localimuQueue4projecting.size(); i++)
			{
				onlymean_time_update(localimuQueue4projecting[i]);
				// Push to localizer queue
				statentime snt;
				snt.header = localimuQueue4projecting[i].header;
				snt.state = xk;
				data->localizerQueue4sync.push_back(snt);
			}

	        geometry_msgs::PoseWithCovarianceStamped P;
	        P.header.stamp = localimuQueue4projecting[localimuQueue4projecting.size()-1].header.stamp;
	        P.header.frame_id = "odom";
	        P.pose.pose.position.x = xk(0);
	        P.pose.pose.position.y = xk(1);
	        P.pose.pose.position.z =  xk(2);
	        P.pose.pose.orientation.w =  xk(6);
	        P.pose.pose.orientation.x =  xk(7);
	        P.pose.pose.orientation.y =  xk(8);
	        P.pose.pose.orientation.z =  xk(9);
	        P.pose.covariance[0] =  covdxk(0,0); P.pose.covariance[1] =  covdxk(0,1); P.pose.covariance[2] =  covdxk(0,2); P.pose.covariance[3] =  covdxk(0,6); P.pose.covariance[4] =  covdxk(0,7); P.pose.covariance[5] =  covdxk(0,8);
	        P.pose.covariance[6] =  covdxk(1,0); P.pose.covariance[7] =  covdxk(1,1); P.pose.covariance[8] =  covdxk(1,2); P.pose.covariance[9] =  covdxk(1,6); P.pose.covariance[10] = covdxk(1,7); P.pose.covariance[11] = covdxk(1,8);
	        P.pose.covariance[12] = covdxk(2,0); P.pose.covariance[13] = covdxk(2,1); P.pose.covariance[14] = covdxk(2,2); P.pose.covariance[15] = covdxk(2,6); P.pose.covariance[16] = covdxk(2,7); P.pose.covariance[17] = covdxk(2,8);
	        P.pose.covariance[18] = covdxk(6,0); P.pose.covariance[19] = covdxk(6,1); P.pose.covariance[20] = covdxk(6,2); P.pose.covariance[21] = covdxk(6,6); P.pose.covariance[22] = covdxk(6,7); P.pose.covariance[23] = covdxk(6,8);
	        P.pose.covariance[24] = covdxk(7,0); P.pose.covariance[25] = covdxk(7,1); P.pose.covariance[26] = covdxk(7,2); P.pose.covariance[27] = covdxk(7,6); P.pose.covariance[28] = covdxk(7,7); P.pose.covariance[29] = covdxk(7,8);
	        P.pose.covariance[30] = covdxk(8,0); P.pose.covariance[31] = covdxk(8,1); P.pose.covariance[32] = covdxk(8,2); P.pose.covariance[33] = covdxk(8,6); P.pose.covariance[34] = covdxk(8,7); P.pose.covariance[35] = covdxk(8,8);

	        pubLocalization.publish(P);

	        // publish tf
			static tf::TransformBroadcaster br;
	        tf::Transform tCur;
	        tCur.setOrigin( tf::Vector3(xk(0),xk(1),xk(2)));
	        tf::Quaternion q;
	        q.setValue(xk(7), xk(8), xk(9), xk(6));
	        tCur.setRotation(q);
	        tf::StampedTransform odom_2_baselink = tf::StampedTransform(tCur, localimuQueue4projecting[localimuQueue4projecting.size()-1].header.stamp, "odom", "base_link");
	        br.sendTransform(odom_2_baselink);

    	}
    }




    void Localizer_nlet::imuHandler(const sensor_msgs::Imu::ConstPtr& imu_msg)
    {

    	sensor_msgs::Imu imu_raw = *imu_msg;


        // rotate acceleration
        Eigen::Vector4d acc(imu_raw.linear_acceleration.x, imu_raw.linear_acceleration.y, imu_raw.linear_acceleration.z, 0);
		acc = data->povTlidar_imu * acc;
        imu_raw.linear_acceleration.x = acc(0);
        imu_raw.linear_acceleration.y = acc(1);
        imu_raw.linear_acceleration.z = acc(2);
        // rotate gyroscope
        Eigen::Vector4d gyr(imu_raw.angular_velocity.x, imu_raw.angular_velocity.y, imu_raw.angular_velocity.z, 0);
        gyr = data->povTlidar_imu * gyr;
        imu_raw.angular_velocity.x = gyr(0);
        imu_raw.angular_velocity.y = gyr(1);
        imu_raw.angular_velocity.z = gyr(2);
        // rotate roll pitch yaw
        Eigen::Quaterniond q_from(imu_raw.orientation.w, imu_raw.orientation.x, imu_raw.orientation.y, imu_raw.orientation.z);
        Eigen::Quaterniond qlidar_ahrs = Eigen::Quaterniond(data->povTlidar_ahrs.block<3,3>(0,0));
        Eigen::Quaterniond q_final = q_from * qlidar_ahrs; // quaternion multiplication == homogeneous matrix multiplication
        // remove yaw from this orientation (because its not suppose to come from IMU, but from GPS)
        double r,p,y;
        data->Quat2ZYXeul(q_final.w(), q_final.x(), q_final.y(), q_final.z(), &r, &p, &y);
        data->ZYXeul2Quat(r, p, 0, &imu_raw.orientation.w, &imu_raw.orientation.x, &imu_raw.orientation.y, &imu_raw.orientation.z);




		localimuQueue4projecting.push_back(imu_raw);
        data->imuQueue4imupreint.push_back(imu_raw);


        if(k==0)
        {
            xnk = Eigen::VectorXd::Zero(16);
            xnk_1 = Eigen::VectorXd::Zero(16);
            dxk = Eigen::VectorXd::Zero(15);
            dxk_1 = Eigen::VectorXd::Zero(15);
            xk = Eigen::VectorXd::Zero(16);
            xk_1 = Eigen::VectorXd::Zero(16);
            xnk(6)=1; xnk_1(6)=1; xk(6)=1; xk_1(6)=1;
            uk = Eigen::VectorXd::Zero(6);
            uk_1 = Eigen::VectorXd::Zero(6);

            covdxk = data->initCov;
            covdxk_1 = data->initCov;

            uk(0) = imu_raw.linear_acceleration.x;
            uk(1) = imu_raw.linear_acceleration.y;
            uk(2) = imu_raw.linear_acceleration.z;
            uk(3) = imu_raw.angular_velocity.x;
            uk(4) = imu_raw.angular_velocity.y;
            uk(5) = imu_raw.angular_velocity.z;


        	xnk(6) = 1;
        	xnk(7) = 0;
        	xnk(8) = 0;
        	xnk(9) = 0;
        	xk(6) = xnk(6);
        	xk(7) = xnk(7);
        	xk(8) = xnk(8);
        	xk(9) = xnk(9);

            // Push to localizer queue
            statentime snt;
            snt.header = imu_raw.header;
            snt.state = xk;
            data->localizerQueue4sync.push_back(snt);
			data->localizerQueue4sm.push_back(snt);

            data->initsynchronize_barrier.unlock();

        }



        else
        {
			MEKF_time_update(imu_raw);
			MEKF_ahrsrollpitch_measurement_update(imu_raw);
			// Push to localizer queue
			statentime snt;
			snt.header = imu_raw.header;
			snt.state = xk;
			data->localizerQueue4sync.push_back(snt);
			data->localizerQueue4sm.push_back(snt);
        }

		k++;




        geometry_msgs::PoseWithCovarianceStamped P;
        P.header.stamp = imu_raw.header.stamp;
        P.header.frame_id = "odom";
        P.pose.pose.position.x = xk(0);
        P.pose.pose.position.y = xk(1);
        P.pose.pose.position.z =  xk(2);
        P.pose.pose.orientation.w =  xk(6);
        P.pose.pose.orientation.x =  xk(7);
        P.pose.pose.orientation.y =  xk(8);
        P.pose.pose.orientation.z =  xk(9);
        P.pose.covariance[0] =  covdxk(0,0); P.pose.covariance[1] =  covdxk(0,1); P.pose.covariance[2] =  covdxk(0,2); P.pose.covariance[3] =  covdxk(0,6); P.pose.covariance[4] =  covdxk(0,7); P.pose.covariance[5] =  covdxk(0,8);
        P.pose.covariance[6] =  covdxk(1,0); P.pose.covariance[7] =  covdxk(1,1); P.pose.covariance[8] =  covdxk(1,2); P.pose.covariance[9] =  covdxk(1,6); P.pose.covariance[10] = covdxk(1,7); P.pose.covariance[11] = covdxk(1,8);
        P.pose.covariance[12] = covdxk(2,0); P.pose.covariance[13] = covdxk(2,1); P.pose.covariance[14] = covdxk(2,2); P.pose.covariance[15] = covdxk(2,6); P.pose.covariance[16] = covdxk(2,7); P.pose.covariance[17] = covdxk(2,8);
        P.pose.covariance[18] = covdxk(6,0); P.pose.covariance[19] = covdxk(6,1); P.pose.covariance[20] = covdxk(6,2); P.pose.covariance[21] = covdxk(6,6); P.pose.covariance[22] = covdxk(6,7); P.pose.covariance[23] = covdxk(6,8);
        P.pose.covariance[24] = covdxk(7,0); P.pose.covariance[25] = covdxk(7,1); P.pose.covariance[26] = covdxk(7,2); P.pose.covariance[27] = covdxk(7,6); P.pose.covariance[28] = covdxk(7,7); P.pose.covariance[29] = covdxk(7,8);
        P.pose.covariance[30] = covdxk(8,0); P.pose.covariance[31] = covdxk(8,1); P.pose.covariance[32] = covdxk(8,2); P.pose.covariance[33] = covdxk(8,6); P.pose.covariance[34] = covdxk(8,7); P.pose.covariance[35] = covdxk(8,8);

        pubLocalization.publish(P);

        // publish tf
		static tf::TransformBroadcaster br;
        tf::Transform tCur;
        tCur.setOrigin( tf::Vector3(xk(0),xk(1),xk(2)));
        tf::Quaternion q;
        q.setValue(xk(7), xk(8), xk(9), xk(6));
        tCur.setRotation(q);
        tf::StampedTransform odom_2_baselink = tf::StampedTransform(tCur, imu_raw.header.stamp, "odom", "base_link");
        br.sendTransform(odom_2_baselink);







    }

    void Localizer_nlet::MEKF_gpspos_measurement_update(nav_msgs::Odometry gps_raw)
    {
    	Eigen::Vector3d p_gps;
    	Eigen::Matrix3d cov_gps;



    	//Extract position and covariance from message
    	p_gps(0) = gps_raw.pose.pose.position.x;
    	p_gps(1) = gps_raw.pose.pose.position.y;
    	p_gps(2) = gps_raw.pose.pose.position.z;

    	cov_gps(0,0)=gps_raw.pose.covariance[0]; cov_gps(0,1)=gps_raw.pose.covariance[1]; cov_gps(0,2)=gps_raw.pose.covariance[2];
    	cov_gps(1,0)=gps_raw.pose.covariance[6]; cov_gps(1,1)=gps_raw.pose.covariance[7]; cov_gps(1,2)=gps_raw.pose.covariance[8];
    	cov_gps(2,0)=gps_raw.pose.covariance[12]; cov_gps(2,1)=gps_raw.pose.covariance[13]; cov_gps(2,2)=gps_raw.pose.covariance[14];

        Eigen::MatrixXd K; // Kalman gain
        std::vector<Eigen::MatrixXd> v;

        // Noise is null
        Eigen::VectorXd wk = Eigen::VectorXd::Zero(6);

        // Linearization of the measurement model
        v = gpos_linearization_discretization(xk, wk, cov_gps);
        Eigen::MatrixXd Hd = v[0];
        Eigen::MatrixXd Nd = v[1];

        K = covdxk*(Hd.transpose())*((Hd*covdxk*(Hd.transpose())+Nd).inverse());
        dxk = K*(p_gps - gpos(xnk, wk)); // we update dxk, not xnk. dxk is not 0 anymore
        covdxk = (Eigen::MatrixXd::Identity(15,15) - K*Hd)*covdxk;


        // inject the error update in nominal state
        xnk.segment(0,6) = xnk.segment(0,6) + dxk.segment(0,6);

        Eigen::Quaterniond qn(xnk(6),xnk(7),xnk(8),xnk(9));
	    double angle = dxk.segment(6,3).norm();
	    Eigen::Vector3d axis = dxk.segment(6,3).normalized();
	    Eigen::Quaterniond qd(Eigen::AngleAxisd(angle, axis)); // NOT EULER see p.22 of paper Quaternion kinematics for the error-state kalman filter
	    Eigen::Quaterniond tmp = qd*qn;
	    xnk(6) = tmp.w();
	    xnk(7) = tmp.x();
	    xnk(8) = tmp.y();
	    xnk(9) = tmp.z();

	    xnk.segment(10,6) = xnk.segment(10,6) + dxk.segment(9,6);

	    // Reset the error state
	    // Since the orientation in dxk is euler angles, we can just put them to 0
	    dxk = Eigen::VectorXd::Zero(15);

        // Compose to obtain full estimate
        xk.segment(0,6) = xnk.segment(0,6) + dxk.segment(0,6);

        Eigen::Quaterniond qn2(xnk(6),xnk(7),xnk(8),xnk(9));
	    double angle2 = dxk.segment(6,3).norm();
	    Eigen::Vector3d axis2 = dxk.segment(6,3).normalized();
	    Eigen::Quaterniond qd2(Eigen::AngleAxisd(angle2, axis2)); // NOT EULER see p.22 of paper Quaternion kinematics for the error-state kalman filter
	    Eigen::Quaterniond tmp2 = qd2*qn2;
	    xk(6) = tmp2.w();
	    xk(7) = tmp2.x();
	    xk(8) = tmp2.y();
	    xk(9) = tmp2.z();

	    xk.segment(10,6) = xnk.segment(10,6) + dxk.segment(9,6);
    }

    void Localizer_nlet::MEKF_gpsyaw_measurement_update(double yawmeas)
    {
    	Eigen::MatrixXd cov_gps(1,1);
    	cov_gps(0) = 0.005236; // TODO!!! THE GPS HEADING COV SHOULD BE FOUND IN GPS DATASHEET

        Eigen::MatrixXd K; // Kalman gain
        std::vector<Eigen::MatrixXd> v;

        // Noise is null
        Eigen::VectorXd wk = Eigen::VectorXd::Zero(6);

        // Linearization of the measurement model
        v = gyaw_linearization_discretization(xk, wk, cov_gps);
        Eigen::MatrixXd Hd = v[0];
        Eigen::MatrixXd Nd = v[1];



        K = covdxk*(Hd.transpose())*((Hd*covdxk*(Hd.transpose())+Nd).inverse());
        if(yawmeas < 0.5 && gyaw(xnk, wk) > 3*M_PI/2) yawmeas += 2*M_PI;
        else if(yawmeas > 3*M_PI/2 && gyaw(xnk, wk) < 0.5) yawmeas -= 2*M_PI;
        dxk = K*(yawmeas - gyaw(xnk, wk)); // we update dxk, not xnk. dxk is not 0 anymore
        covdxk = (Eigen::MatrixXd::Identity(15,15) - K*Hd)*covdxk;


        // inject the error update in nominal state
        xnk.segment(0,6) = xnk.segment(0,6) + dxk.segment(0,6);

        Eigen::Quaterniond qn(xnk(6),xnk(7),xnk(8),xnk(9));
	    double angle = dxk.segment(6,3).norm();
	    Eigen::Vector3d axis = dxk.segment(6,3).normalized();
	    Eigen::Quaterniond qd(Eigen::AngleAxisd(angle, axis)); // NOT EULER see p.22 of paper Quaternion kinematics for the error-state kalman filter
	    Eigen::Quaterniond tmp = qd*qn;
	    xnk(6) = tmp.w();
	    xnk(7) = tmp.x();
	    xnk(8) = tmp.y();
	    xnk(9) = tmp.z();

	    xnk.segment(10,6) = xnk.segment(10,6) + dxk.segment(9,6);

	    // Reset the error state
	    // Since the orientation in dxk is euler angles, we can just put them to 0
	    dxk = Eigen::VectorXd::Zero(15);

        // Compose to obtain full estimate
        xk.segment(0,6) = xnk.segment(0,6) + dxk.segment(0,6);

        Eigen::Quaterniond qn2(xnk(6),xnk(7),xnk(8),xnk(9));
	    double angle2 = dxk.segment(6,3).norm();
	    Eigen::Vector3d axis2 = dxk.segment(6,3).normalized();
	    Eigen::Quaterniond qd2(Eigen::AngleAxisd(angle2, axis2)); // NOT EULER see p.22 of paper Quaternion kinematics for the error-state kalman filter
	    Eigen::Quaterniond tmp2 = qd2*qn2;
	    xk(6) = tmp2.w();
	    xk(7) = tmp2.x();
	    xk(8) = tmp2.y();
	    xk(9) = tmp2.z();

	    xk.segment(10,6) = xnk.segment(10,6) + dxk.segment(9,6);
    }

    void Localizer_nlet::MEKF_ahrsrollpitch_measurement_update(sensor_msgs::Imu imu_raw)
    {
        Eigen::Vector2d rpmeas;
        double dummy;
        data->Quat2ZYXeul(imu_raw.orientation.w, imu_raw.orientation.x, imu_raw.orientation.y, imu_raw.orientation.z, &rpmeas(0), &rpmeas(1), &dummy);

    	Eigen::Matrix2d cov_imu;


    	cov_imu(0,0)=0.005236; cov_imu(0,1)=0;
    	cov_imu(1,0)=0; cov_imu(1,1)=0.005236;


        Eigen::MatrixXd K; // Kalman gain
        std::vector<Eigen::MatrixXd> v;

        // Noise is null
        Eigen::VectorXd wk = Eigen::VectorXd::Zero(6);

        // Linearization of the measurement model
        v = grollpitch_linearization_discretization(xk, wk, cov_imu);
        Eigen::MatrixXd Hd = v[0];
        Eigen::MatrixXd Nd = v[1];


        K = covdxk*(Hd.transpose())*((Hd*covdxk*(Hd.transpose())+Nd).inverse());
        dxk = K*(rpmeas - grollpitch(xnk, wk)); // we update dxk, not xnk. dxk is not 0 anymore
        covdxk = (Eigen::MatrixXd::Identity(15,15) - K*Hd)*covdxk;


        // inject the error update in nominal state
        xnk.segment(0,6) = xnk.segment(0,6) + dxk.segment(0,6);

        Eigen::Quaterniond qn(xnk(6),xnk(7),xnk(8),xnk(9));
	    double angle = dxk.segment(6,3).norm();
	    Eigen::Vector3d axis = dxk.segment(6,3).normalized();
	    Eigen::Quaterniond qd(Eigen::AngleAxisd(angle, axis)); // NOT EULER see p.22 of paper Quaternion kinematics for the error-state kalman filter
	    Eigen::Quaterniond tmp = qd*qn;
	    xnk(6) = tmp.w();
	    xnk(7) = tmp.x();
	    xnk(8) = tmp.y();
	    xnk(9) = tmp.z();

	    xnk.segment(10,6) = xnk.segment(10,6) + dxk.segment(9,6);



	    // Reset the error state
	    // Since the orientation in dxk is euler angles, we can just put them to 0
	    dxk = Eigen::VectorXd::Zero(15);

        // Compose to obtain full estimate
        xk.segment(0,6) = xnk.segment(0,6) + dxk.segment(0,6);

        Eigen::Quaterniond qn2(xnk(6),xnk(7),xnk(8),xnk(9));
	    double angle2 = dxk.segment(6,3).norm();
	    Eigen::Vector3d axis2 = dxk.segment(6,3).normalized();
	    Eigen::Quaterniond qd2(Eigen::AngleAxisd(angle2, axis2)); // NOT EULER see p.22 of paper Quaternion kinematics for the error-state kalman filter
	    Eigen::Quaterniond tmp2 = qd2*qn2;
	    xk(6) = tmp2.w();
	    xk(7) = tmp2.x();
	    xk(8) = tmp2.y();
	    xk(9) = tmp2.z();

	    xk.segment(10,6) = xnk.segment(10,6) + dxk.segment(9,6);

//		std::cout << "after = " << std::endl << grollpitch(xk, wk) << std::endl;
    }


    void Localizer_nlet::MEKF_time_update(sensor_msgs::Imu imu_raw)
    {

    	xnk_1 = xnk;
		dxk_1 = dxk;
		xk_1 = xk;
		covdxk_1 = covdxk;
		uk_1 = uk;


        uk(0) = imu_raw.linear_acceleration.x;
        uk(1) = imu_raw.linear_acceleration.y;
        uk(2) = imu_raw.linear_acceleration.z;
        uk(3) = imu_raw.angular_velocity.x;
        uk(4) = imu_raw.angular_velocity.y;
        uk(5) = imu_raw.angular_velocity.z;




		// Noise is null
		Eigen::VectorXd wk_1 = Eigen::VectorXd::Zero(12);

		// Linearization and discretization of the motion model
		std::vector<Eigen::MatrixXd> v = f_delta(xnk_1, uk_1); // TODO do we really use the previous state as linearization point OR the current state?
		Eigen::MatrixXd Ad = v[0];
		Eigen::MatrixXd Md = v[1];
		Eigen::MatrixXd Qd = v[2];

		// Propagate mean using Euler's method
		xnk = f_nominal(xnk_1, uk_1);
		dxk = Ad*dxk_1; // should always be = 0
		// Propagate covariance
		covdxk = Ad*covdxk_1*(Ad.transpose())+Md*Qd*(Md.transpose());




//    	    // Gravity vector update
//    	    if(abs(uk.segment(0,3).norm()-9.81) < 0.01)
//    	    {
//    	    	std::cout << "GRAVITY UPDATE" << std::endl;
//    	    	Eigen::MatrixXd K; // Kalman gain
//				std::vector<Eigen::MatrixXd> v4;
//
//				// Noise is null
//				Eigen::VectorXd wk = Eigen::VectorXd::Zero(6);
//
//				// Linearization of the measurement model
//				v4 = h_linearization_discretization(xnk, wk);
//				Eigen::MatrixXd Hd = v4[0];
//				Eigen::MatrixXd Nd = v4[1];
//
//				K = covdxk*(Hd.transpose())*((Hd*covdxk*(Hd.transpose())+Nd).inverse());
//				dxk = K*(uk.segment(0,3) - h(xnk, wk)); // we update dxk, not xnk. dxk is not 0 anymore
//				std::cout << "uk = " << uk.segment(0,3) << std::endl;
//				std::cout << "prediction = " << h(xnk,wk) << std::endl;
//				std::cout << "dxk = " << dxk << std::endl;
//
//				covdxk = (Eigen::MatrixXd::Identity(15,15) - K*Hd)*covdxk;
//
//
//				// inject the error update in nominal state
//				xnk.segment(0,6) = xnk.segment(0,6) + dxk.segment(0,6);
//
//				Eigen::Quaterniond qn(xnk(6),xnk(7),xnk(8),xnk(9));
//				double angle = dxk.segment(6,3).norm();
//				Eigen::Vector3d axis = dxk.segment(6,3).normalized();
//				Eigen::Quaterniond qd(Eigen::AngleAxisd(angle, axis)); // see p.22 of paper Quaternion kinematics for the error-state kalman filter
//				Eigen::Quaterniond tmp = qd*qn;
//				xnk(6) = tmp.w();
//				xnk(7) = tmp.x();
//				xnk(8) = tmp.y();
//				xnk(9) = tmp.z();
//
//				xnk.segment(10,6) = xnk.segment(10,6) + dxk.segment(9,6);
//
//				// Reset the error state
//				// Since the orientation in dxk is euler angles, we can just put them to 0
//				dxk = Eigen::VectorXd::Zero(15);
//
//    	    }


		// Compose to obtain full estimate
		xk.segment(0,6) = xnk.segment(0,6) + dxk.segment(0,6);

		Eigen::Quaterniond qn(xnk(6),xnk(7),xnk(8),xnk(9));
		double angle = dxk.segment(6,3).norm();
		Eigen::Vector3d axis = dxk.segment(6,3).normalized();
		Eigen::Quaterniond qd(Eigen::AngleAxisd(angle, axis)); // NOT EULER see p.22 of paper Quaternion kinematics for the error-state kalman filter
		Eigen::Quaterniond tmp = qd*qn;
		xk(6) = tmp.w();
		xk(7) = tmp.x();
		xk(8) = tmp.y();
		xk(9) = tmp.z();

		xk.segment(10,6) = xnk.segment(10,6) + dxk.segment(9,6);




    }




    void Localizer_nlet::onlymean_time_update(sensor_msgs::Imu imu_raw)
    {

    	xnk_1 = xnk;
		dxk_1 = dxk;
		xk_1 = xk;
		covdxk_1 = covdxk;
		uk_1 = uk;


        uk(0) = imu_raw.linear_acceleration.x;
        uk(1) = imu_raw.linear_acceleration.y;
        uk(2) = imu_raw.linear_acceleration.z;
        uk(3) = imu_raw.angular_velocity.x;
        uk(4) = imu_raw.angular_velocity.y;
        uk(5) = imu_raw.angular_velocity.z;




		// Noise is null
		Eigen::VectorXd wk_1 = Eigen::VectorXd::Zero(12);

		// Propagate mean using Euler's method
		xnk = f_nominal(xnk_1, uk_1);

		// Compose to obtain full estimate
		xk.segment(0,6) = xnk.segment(0,6) + dxk.segment(0,6);

		Eigen::Quaterniond qn(xnk(6),xnk(7),xnk(8),xnk(9));
		double angle = dxk.segment(6,3).norm();
		Eigen::Vector3d axis = dxk.segment(6,3).normalized();
		Eigen::Quaterniond qd(Eigen::AngleAxisd(angle, axis)); // NOT EULER see p.22 of paper Quaternion kinematics for the error-state kalman filter
		Eigen::Quaterniond tmp = qd*qn;
		xk(6) = tmp.w();
		xk(7) = tmp.x();
		xk(8) = tmp.y();
		xk(9) = tmp.z();

		xk.segment(10,6) = xnk.segment(10,6) + dxk.segment(9,6);

    }




    Eigen::VectorXd Localizer_nlet::f_nominal(Eigen::VectorXd xnlk_1, Eigen::VectorXd umeask_1)
    {
    	//x(0) =  px
    	//x(1) =  py
    	//x(2) =  pz
    	//x(3) =  vx
    	//x(4) =  vy
    	//x(5) =  vz
    	//x(6) =  qw
    	//x(7) =  qx
    	//x(8) =  qy
    	//x(9) =  qz
    	//x(10) = bax
    	//x(11) = bay
    	//x(12) = baz
    	//x(13) = bgx
    	//x(14) = bgy
    	//x(15) = bgz

    	//u(0) =  fx
    	//u(1) =  fy
    	//u(2) =  fz
    	//u(3) =  gx
    	//u(4) =  gy
    	//u(5) =  gz

    	//w(0) =  wax
    	//w(1) =  way
    	//w(2) =  waz
    	//w(3) =  wgx
    	//w(4) =  wgy
    	//w(5) =  wgz
    	//w(6) =  wbax
    	//w(7) =  wbay
    	//w(8) =  wbaz
    	//w(9) =  wbgx
    	//w(10) = wbgy
    	//w(11) = wbgz

    	Eigen::VectorXd xnlk(16);
        double q0 = xnlk_1(6); //qw
        double q1 = xnlk_1(7); //qx
        double q2 = xnlk_1(8); //qy
        double q3 = xnlk_1(9); //qz
        Eigen::Vector3d g;
        g << 0,0,-9.81;
        double h = 1.0/data->imu_rate;


        Eigen::MatrixXd Rw_b(3,3);
        Rw_b << pow(q0,2)+pow(q1,2)-pow(q2,2)-pow(q3,2), 2*(q1*q2-q0*q3), 2*(q1*q3+q0*q2),
        		2*(q1*q2+q0*q3), pow(q0,2)-pow(q1,2)+pow(q2,2)-pow(q3,2), 2*(q2*q3-q0*q1),
				2*(q1*q3-q0*q2), 2*(q2*q3+q0*q1), pow(q0,2)-pow(q1,2)-pow(q2,2)+pow(q3,2);





    	// Position
        xnlk.segment(0,3) = xnlk_1.segment(0,3) + h*xnlk_1.segment(3,3) + 0.5*pow(h,2)*(Rw_b*umeask_1.segment(0,3)-Rw_b*xnlk_1.segment(10,3)+g);

        // Velocity
        xnlk.segment(3,3) = xnlk_1.segment(3,3) + h*(Rw_b*umeask_1.segment(0,3)-Rw_b*xnlk_1.segment(10,3)+g);

        // Attitude
        Eigen::Quaterniond qn(q0,q1,q2,q3);
        Eigen::Vector3d wh = (umeask_1.segment(3,3)-xnlk_1.segment(13,3))*h;
	    double angle = wh.norm();
	    Eigen::Vector3d axis = wh.normalized();
	    Eigen::Quaterniond qwh(Eigen::AngleAxisd(angle, axis)); // see p.22 of paper Quaternion kinematics for the error-state kalman filter
	    Eigen::Quaterniond tmp = qn*qwh;
	    tmp.normalize(); // IMPORTANT
	    xnlk(6) = tmp.w();
	    xnlk(7) = tmp.x();
	    xnlk(8) = tmp.y();
	    xnlk(9) = tmp.z();



        // Bias
        xnlk.segment(10,6) = (Eigen::MatrixXd::Identity(6,6))*xnlk_1.segment(10,6);



        return xnlk;

    }
    std::vector<Eigen::MatrixXd> Localizer_nlet::f_delta(Eigen::VectorXd xnlk_1, Eigen::VectorXd umeask_1)
    {
        Eigen::MatrixXd Ad(15,15);
        Eigen::MatrixXd Md(15,12);
        Eigen::MatrixXd Qd(12,12);
        std::vector<Eigen::MatrixXd> v;
        double q0 = xnlk_1(6); //qw
        double q1 = xnlk_1(7); //qx
        double q2 = xnlk_1(8); //qy
        double q3 = xnlk_1(9); //qz
        double h = 1.0/data->imu_rate;

        Eigen::Matrix3d Rn_b;
        Rn_b << pow(q0,2)+pow(q1,2)-pow(q2,2)-pow(q3,2), 2*(q1*q2-q0*q3), 2*(q1*q3+q0*q2),
        		2*(q1*q2+q0*q3), pow(q0,2)-pow(q1,2)+pow(q2,2)-pow(q3,2), 2*(q2*q3-q0*q1),
				2*(q1*q3-q0*q2), 2*(q2*q3+q0*q1), pow(q0,2)-pow(q1,2)-pow(q2,2)+pow(q3,2);
        Eigen::Vector3d Ra = Rn_b*(umeask_1.segment(0,3)-xnlk_1.segment(10,3));
        Eigen::Matrix3d Ax;
        Ax << 0, -Ra(2), Ra(1),
        	  Ra(2), 0, -Ra(0),
			  -Ra(1), Ra(0), 0;


        Ad << Eigen::MatrixXd::Identity(3,3), h*Eigen::MatrixXd::Identity(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3),
        	  Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Identity(3,3), -h*Ax, -h*Rn_b, Eigen::MatrixXd::Zero(3,3),
			  Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Identity(3,3), Eigen::MatrixXd::Zero(3,3), -h*Rn_b,
			  Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Identity(3,3), Eigen::MatrixXd::Zero(3,3),
			  Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Identity(3,3);

        Md << Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3),
        	  Eigen::MatrixXd::Identity(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3),
			  Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Identity(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3),
			  Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Identity(3,3), Eigen::MatrixXd::Zero(3,3),
			  Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Zero(3,3), Eigen::MatrixXd::Identity(3,3);

        Qd << data->accPSD, Eigen::MatrixXd::Zero(3,9),
        	  Eigen::MatrixXd::Zero(3,3), data->gyrPSD, Eigen::MatrixXd::Zero(3,6),
			  Eigen::MatrixXd::Zero(6,6), data->biasPSD;

        v.push_back(Ad);
        v.push_back(Md);
        v.push_back(Qd);

        return v;
    }



    Eigen::Vector3d Localizer_nlet::gpos(Eigen::VectorXd x, Eigen::VectorXd w)
    {
        Eigen::Vector3d g;
        g(0) = x(0) + w(0);
        g(1) = x(1) + w(1);
        g(2) = x(2) + w(2);
        return g;
    }


    // See p.63 of paper Quaternion kinematics for the error-state kalman filter
    std::vector<Eigen::MatrixXd> Localizer_nlet::gpos_linearization_discretization(Eigen::VectorXd xk, Eigen::VectorXd wk, Eigen::MatrixXd Nin)
    {
        Eigen::MatrixXd Hx(3,16);
        Eigen::MatrixXd Hq(16,15);
        Eigen::MatrixXd Hd(3,15);
        Eigen::MatrixXd Nd(3,3);
        std::vector<Eigen::MatrixXd> v;

        double qw = xk(6); //qw
        double qx = xk(7); //qx
        double qy = xk(8); //qy
        double qz = xk(9); //qz

        Eigen::MatrixXd Q_deltatheta(4,3);
        Q_deltatheta << -qx, -qy, -qz,
        				qw, qz, -qy,
						-qz, qw, qx,
						qy, -qx, qw;
        Q_deltatheta = 0.5*Q_deltatheta;


        Hx << 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        	  0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			  0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;

        Hq << Eigen::MatrixXd::Identity(6,6), Eigen::MatrixXd::Zero(6,9),
        	  Eigen::MatrixXd::Zero(4,6), Q_deltatheta, Eigen::MatrixXd::Zero(4,6),
			  Eigen::MatrixXd::Zero(6,9), Eigen::MatrixXd::Identity(6,6);


        Hd = Hx*Hq;

        Nd = Nin;

        v.push_back(Hd);
        v.push_back(Nd);

        return v;
    }


    Eigen::Vector3d Localizer_nlet::ggravity(Eigen::VectorXd x, Eigen::VectorXd w)
    {
    	Eigen::Vector3d h;
        double q0 = x(6); //qw
        double q1 = x(7); //qx
        double q2 = x(8); //qy
        double q3 = x(9); //qz
        Eigen::Vector3d g;
        g << 0,0,-9.81;

        Eigen::Matrix3d Rn_b;
        Rn_b << pow(q0,2)+pow(q1,2)-pow(q2,2)-pow(q3,2), 2*(q1*q2-q0*q3), 2*(q1*q3+q0*q2),
        		2*(q1*q2+q0*q3), pow(q0,2)-pow(q1,2)+pow(q2,2)-pow(q3,2), 2*(q2*q3-q0*q1),
				2*(q1*q3-q0*q2), 2*(q2*q3+q0*q1), pow(q0,2)-pow(q1,2)-pow(q2,2)+pow(q3,2);
        h = -Rn_b.inverse()*g + x.segment(10,3);

        return h;

    }


    // See p.63 of paper Quaternion kinematics for the error-state kalman filter
    std::vector<Eigen::MatrixXd> Localizer_nlet::ggravity_linearization_discretization(Eigen::VectorXd xk, Eigen::VectorXd wk)
    {
        Eigen::MatrixXd Hd(3,15);
        Eigen::MatrixXd Nd(3,3);
        std::vector<Eigen::MatrixXd> v;

        double q0 = xk(6); //qw
        double q1 = xk(7); //qx
        double q2 = xk(8); //qy
        double q3 = xk(9); //qz
        Eigen::Matrix3d Gx;
        Gx << 0, 9.81,0,
        	 -9.81, 0, 0,
             0, 0, 0;

        Eigen::Matrix3d Rn_b;
        Rn_b << pow(q0,2)+pow(q1,2)-pow(q2,2)-pow(q3,2), 2*(q1*q2-q0*q3), 2*(q1*q3+q0*q2),
        		2*(q1*q2+q0*q3), pow(q0,2)-pow(q1,2)+pow(q2,2)-pow(q3,2), 2*(q2*q3-q0*q1),
				2*(q1*q3-q0*q2), 2*(q2*q3+q0*q1), pow(q0,2)-pow(q1,2)-pow(q2,2)+pow(q3,2);

        Hd << -Rn_b.inverse()*Gx, Eigen::MatrixXd::Zero(3,9), Eigen::MatrixXd::Identity(3,3);

        Nd = data->accPSD;




        v.push_back(Hd);
        v.push_back(Nd);

        return v;
    }

    double Localizer_nlet::gyaw(Eigen::VectorXd x, Eigen::VectorXd w)
    {
//        double q0 = x(6); //qw
//        double q1 = x(7); //qx
//        double q2 = x(8); //qy
//        double q3 = x(9); //qz
        double roll;
        double pitch;
        double yaw;
//        g = atan2(2*(q0*q3+q1*q2), 1-2*(pow(q2,2)+pow(q3,2)));
        data->Quat2ZYXeul(x(6), x(7), x(8), x(9), &roll, &pitch, &yaw);

        return yaw;
    }


    // See p.63 of paper Quaternion kinematics for the error-state kalman filter
    std::vector<Eigen::MatrixXd> Localizer_nlet::gyaw_linearization_discretization(Eigen::VectorXd xk, Eigen::VectorXd wk, Eigen::MatrixXd Nin)
    {
        Eigen::MatrixXd Hx(1,16);
        Eigen::MatrixXd Hq(16,15);
        Eigen::MatrixXd Hd(3,15);
        Eigen::MatrixXd Nd(1,1);
        std::vector<Eigen::MatrixXd> v;

        double q0 = xk(6); //qw
        double q1 = xk(7); //qx
        double q2 = xk(8); //qy
        double q3 = xk(9); //qz

        Eigen::MatrixXd Q_deltatheta(4,3);
        Q_deltatheta << -q1, -q2, -q3,
        				q0, q3, -q2,
						-q3, q0, q1,
						q2, -q1, q0;
        Q_deltatheta = 0.5*Q_deltatheta;


        //dif(atan2) = dif(atan) https://en.wikipedia.org/wiki/Atan2#Derivative
        Hx << 0, 0, 0, 0, 0, 0,
        						(-2*(2*pow(q2,2)+2*pow(q3,2)-1)*q3)/(4*pow(q0,2)*pow(q3,2)+8*q0*q1*q2*q3+4*pow(q1,2)*pow(q2,2)+pow((2*pow(q2,2)+2*pow(q3,2)-1),2)),
								(-2*q2*(2*pow(q2,2)+2*pow(q3,2)-1))/(4*pow(q1,2)*pow(q2,2)+8*q0*q1*q2*q3+4*pow(q0,2)*pow(q3,2)+pow((2*pow(q2,2)+2*pow(q3,2)-1),2)),
								(2*(2*pow(q2,2)*q1+4*q2*q0*q3-q1*(2*pow(q3,2)-1)))/(4*pow(q2,4)+4*pow(q2,2)*(pow(q1,2)+2*pow(q3,2)-1)+8*q0*q1*q2*q3+4*pow(q0,2)*pow(q3,2)+pow((2*pow(q3,2)-1),2)),
								(2*(2*pow(q3,2)*q0+4*q3*q1*q2-q0*(2*pow(q2,2)-1)))/(4*pow(q3,4)+4*pow(q3,2)*(pow(q0,2)+2*pow(q2,2)-1)+8*q0*q1*q2*q3+4*pow(q1,2)*pow(q2,2)+pow((2*pow(q2,2)-1),2)),
			  0, 0, 0, 0, 0, 0;


        Hq << Eigen::MatrixXd::Identity(6,6), Eigen::MatrixXd::Zero(6,9),
        	  Eigen::MatrixXd::Zero(4,6), Q_deltatheta, Eigen::MatrixXd::Zero(4,6),
			  Eigen::MatrixXd::Zero(6,9), Eigen::MatrixXd::Identity(6,6);


        Hd = Hx*Hq;

        Nd = Nin;

        v.push_back(Hd);
        v.push_back(Nd);

        return v;
    }


    Eigen::Vector2d Localizer_nlet::grollpitch(Eigen::VectorXd x, Eigen::VectorXd w)
    {
        Eigen::Vector2d rollpitch;
        double yaw;

        data->Quat2ZYXeul(x(6), x(7), x(8), x(9), &rollpitch(0), &rollpitch(1), &yaw);

        return rollpitch;
    }


    // See p.63 of paper Quaternion kinematics for the error-state kalman filter
    std::vector<Eigen::MatrixXd> Localizer_nlet::grollpitch_linearization_discretization(Eigen::VectorXd xk, Eigen::VectorXd wk, Eigen::MatrixXd Nin)
    {
        Eigen::MatrixXd Hx(2,16);
        Eigen::MatrixXd Hq(16,15);
        Eigen::MatrixXd Hd(3,15);
        Eigen::MatrixXd Nd(2,2);
        std::vector<Eigen::MatrixXd> v;

        double q0 = xk(6); //qw
        double q1 = xk(7); //qx
        double q2 = xk(8); //qy
        double q3 = xk(9); //qz

        Eigen::MatrixXd Q_deltatheta(4,3);
        Q_deltatheta << -q1, -q2, -q3,
        				q0, q3, -q2,
						-q3, q0, q1,
						q2, -q1, q0;
        Q_deltatheta = 0.5*Q_deltatheta;


        //dif(atan2) = dif(atan) https://en.wikipedia.org/wiki/Atan2#Derivative
        Hx << 0, 0, 0, 0, 0, 0,
		        (-2*q1*(2*pow(q1,2)+2*pow(q2,2)-1))/(4*pow(q0,2)*pow(q1,2)+8*q0*q1*q2*q3+4*pow(q1,4)+4*pow(q1,2)*(2*pow(q2,2)-1)+4*pow(q2,4)+4*pow(q2,2)*(pow(q3,2)-1)+1),
		        (2*(2*pow(q1,2)*q0+4*q1*q2*q3-q0*(2*pow(q2,2)-1)))/(4*pow(q1,4)+4*pow(q1,2)*(pow(q0,2)+2*pow(q2,2)-1)+8*q0*q1*q2*q3+4*pow(q2,4)+4*pow(q2,2)*(pow(q3,2)-1)+1),
		        (2*(2*pow(q2,2)*q3+4*q2*q0*q1-(2*pow(q1,2)-1)*q3))/(4*pow(q2,4)+4*pow(q2,2)*(2*pow(q1,2)+pow(q3,2)-1)+8*q0*q1*q2*q3+4*pow(q0,2)*pow(q1,2)+pow((2*pow(q1,2)-1),2)),
		        (-2*(2*pow(q1,2)+2*pow(q2,2)-1)*q2)/(4*pow(q3,2)*pow(q2,2)+8*q0*q1*q2*q3+4*pow(q0,2)*pow(q1,2)+pow((2*pow(q1,2)+2*pow(q2,2)-1),2)),
				  	0, 0, 0, 0, 0, 0,
			  0, 0, 0, 0, 0, 0,
				(2*q2)/(sqrt(-4*pow(q0,2)*pow(q2,2)+8*q0*q1*q2*q3-(2*q1*q3-1)*(2*q1*q3+1))),
				(-2*q3)/(sqrt(-4*pow(q1,2)*pow(q3,2)+8*q0*q1*q2*q3-(2*q0*q2-1)*(2*q0*q2+1))),
				(2*q0)/(sqrt(-4*pow(q2,2)*pow(q0,2)+8*q0*q1*q2*q3-(2*q1*q3-1)*(2*q1*q3+1))),
				(-2*q1)/(sqrt(-4*pow(q3,2)*pow(q1,2)+8*q0*q1*q2*q3-(2*q0*q2-1)*(2*q0*q2+1))),
					0, 0, 0, 0, 0, 0;


        Hq << Eigen::MatrixXd::Identity(6,6), Eigen::MatrixXd::Zero(6,9),
        	  Eigen::MatrixXd::Zero(4,6), Q_deltatheta, Eigen::MatrixXd::Zero(4,6),
			  Eigen::MatrixXd::Zero(6,9), Eigen::MatrixXd::Identity(6,6);


        Hd = Hx*Hq;

        Nd = Nin;

        v.push_back(Hd);
        v.push_back(Nd);

        return v;
    }


    PLUGINLIB_EXPORT_CLASS(nodelet_ns::Localizer_nlet, nodelet::Nodelet);
}
