#include <data_sglton.h>



// This is not the declaration of a Singleton object; we are setting 
// the private pointer variable "static Singleton *s_Instance" to 0.
// In java, we would simply set the pointer to 0 at declaration, but
// c++ don't allow that.
// We also couldn't put it in the constructor since "get" function is
// called before the constructor, and "get" need to see if it's 0 or not
Data_sglton *Data_sglton::s_Instance = 0;


// Constructor is only called once when *Singleton::get() is called the first time.
Data_sglton::Data_sglton()
{
	n_node = 0;
	n_node_opt = 0;
	optimize_barrier.lock();
	loopcloser_barrier.lock();
	initsynchronize_barrier.lock();

    // ISAM2 initialization
    gtsam::ISAM2Params parameters;
    parameters.relinearizeThreshold = 0.1;
    parameters.relinearizeSkip = 1;
    isam = new gtsam::ISAM2(parameters);

    lastCovariance = Eigen::MatrixXd::Zero(15,15);
    localMapCorner.reset(new pcl::PointCloud<pcl::PointXYZI>());
    localMapSurf.reset(new pcl::PointCloud<pcl::PointXYZI>());

    opt_flag = false;
    opt_flag2 = true;
    sync_flag = false;

    localMapN = 20;
    Nr = 20; // Number of rings in the SC matrix ; 20 in the original paper (IROS 18)
    Ns = 60; // Number of sectors in the SC matrix ; 60 in the original paper (IROS 18)
    Lmax = 80.0; // 80 meter max in the original paper (IROS 18)
    first_stage_ncandidates = 3; // 10 is enough. (refer the IROS 18 paper)


    /*
    ////////////////////// MIT ///////////////////////////

    accPSD << 1.595169e-5,          0,          0,
   	   	   	  	 	    0,1.595169e-5,          0,
						0,          0,1.595169e-5; // acc white noise in continuous // imuAccNoise = 3.9939570888238808e-03
    gyrPSD << 2.444952e-6,          0,          0,
						0,2.444952e-6,          0,
						0,          0,2.444952e-6; // gyro white noise in continuous // imuGyrNoise = 1.5636343949698187e-03

    // TODO!!!! biasPSD
    biasPSD = Eigen::MatrixXd::Zero(6,6);
    biasPSD << 4.1418e-09,   0,   0,   0,   0,   0,
    			  0,4.1418e-09,   0,   0,   0,   0,
    		      0,   0,4.1418e-09,   0,   0,   0,
    			  0,   0,   0,1.2702e-09,   0,   0,
    			  0,   0,   0,   0,1.2702e-09,   0,
				  0,   0,   0,   0,   0,1.2702e-09;

    Q = Eigen::MatrixXd::Zero(12,12);
    Q << accPSD, Eigen::MatrixXd::Zero(3,9),
    	 Eigen::MatrixXd::Zero(3,3), gyrPSD, Eigen::MatrixXd::Zero(3,6),
		 Eigen::MatrixXd::Zero(6,6), biasPSD;

    initPoseCov = Eigen::MatrixXd::Zero(6,6);
    initPoseCov << 1e-4,   0,   0,   0,   0,   0,
			   	   	  0,1e-4,   0,   0,   0,   0,
					  0,   0,1e-4,   0,   0,   0,
					  0,   0,   0,1e-4,   0,   0,
					  0,   0,   0,   0,1e-4,   0,
					  0,   0,   0,   0,   0,1e-4;

    initVelCov = Eigen::MatrixXd::Zero(3,3);
	initVelCov  << 1e-4,   0,  0,
					  0,1e-4,  0,
					  0,  0,1e-4; // m/s

	initBiasCov = Eigen::MatrixXd::Zero(6,6);
	initBiasCov << 1e-6,   0,   0,   0,   0,   0,
				  	  0,1e-6,   0,   0,   0,   0,
					  0,   0,1e-6,   0,   0,   0,
					  0,   0,   0,1e-6,   0,   0,
					  0,   0,   0,   0,1e-6,   0,
					  0,   0,   0,   0,   0,1e-6; // 1e-6 seems to be good

	initCov = Eigen::MatrixXd::Zero(15,15);
	initCov << initPoseCov, Eigen::MatrixXd::Zero(6,9),
			   Eigen::MatrixXd::Zero(3,6), initVelCov, Eigen::MatrixXd::Zero(3,6),
			   Eigen::MatrixXd::Zero(6,9), initBiasCov;

    /////////// Choosed by trial and error ///////////////

    integrationPSD << 1e-8,   0,   0,
    					 0,1e-8,   0,
						 0,   0,1e-8; // error committed in integrating position from velocities, 1e-8 seems good

	// Rotation around y=-pi
	povTlidar_imu << -1,  0,  0,  0,
					  0,  1,  0,  0,
					  0,  0, -1,  0,
					  0,  0,  0,  1;
	// Rotation around z=-pi/2
	povTlidar_ahrs <<  0,  1,  0,  0,
					  -1,  0,  0,  0,
					   0,  0,  1,  0,
					   0,  0,  0,  1;

	sm_cov = 1e-2;

	sync_rate = 1;

	imu_rate = 500;

	nrow_scan = 16;
	ncol_scan = 1800;


	// ALSO, REMOVE
	// compass + AHRS constraint in synchronizer
	// MEKF_ahrsrollpitch_measurement_update in localizer
	// heading in position covariance in gps_nlet
	// inverse 9.81 in localizer
	// add -9.81 to p in synchronizer
	// change nrow_scan and ncol_scan
	// remove >100 for removing outlier points in scanmatcher
	// remove pitch_cal in scanmatcher

    /////////////////////////////////////////////////////////////////

	*/

    ////////////////////// MISSION MASTER ///////////////////////////

    accPSD << 0.004162,          0,          0,
   	   	   	  	 	    0,0.004162,          0, // PSD IS LIKE VARIANCE
						0,          0,0.004162; // acc white noise in continuous // imuAccNoise = 3.9939570888238808e-03
    gyrPSD << 0.0012341,          0,          0,
						0,0.0012341,          0,
						0,          0,0.0012341; // gyro white noise in continuous // imuGyrNoise = 1.5636343949698187e-03

    // TODO!!!! biasPSD
    biasPSD = Eigen::MatrixXd::Zero(6,6);
    biasPSD << 4.1418e-09,   0,   0,   0,   0,   0,
    			  0,4.1418e-09,   0,   0,   0,   0,
    		      0,   0,4.1418e-09,   0,   0,   0,
    			  0,   0,   0,1.2702e-09,   0,   0,
    			  0,   0,   0,   0,1.2702e-09,   0,
				  0,   0,   0,   0,   0,1.2702e-09;

    Q = Eigen::MatrixXd::Zero(12,12);
    Q << accPSD, Eigen::MatrixXd::Zero(3,9),
    	 Eigen::MatrixXd::Zero(3,3), gyrPSD, Eigen::MatrixXd::Zero(3,6),
		 Eigen::MatrixXd::Zero(6,6), biasPSD;

    initPoseCov = Eigen::MatrixXd::Zero(6,6);
    initPoseCov << 1e-4,   0,   0,   0,   0,   0,
			   	   	  0,1e-4,   0,   0,   0,   0,
					  0,   0,1e-4,   0,   0,   0,
					  0,   0,   0,1e-4,   0,   0,
					  0,   0,   0,   0,1e-4,   0,
					  0,   0,   0,   0,   0,1e-4;

    initVelCov = Eigen::MatrixXd::Zero(3,3);
	initVelCov  << 1e-4,   0,  0,
					  0,1e-4,  0,
					  0,  0,1e-4; // m/s

	initBiasCov = Eigen::MatrixXd::Zero(6,6);
	initBiasCov << 0.00349,   0,   0,   0,   0,   0,
				  	  0,0.00349,   0,   0,   0,   0,
					  0,   0,0.00349,   0,   0,   0,
					  0,   0,   0,0.00349,   0,   0,
					  0,   0,   0,   0,0.00349,   0,
					  0,   0,   0,   0,   0,0.00349; // i choose 1 because theres is a big bias at beginning

	initCov = Eigen::MatrixXd::Zero(15,15);
	initCov << initPoseCov, Eigen::MatrixXd::Zero(6,9),
			   Eigen::MatrixXd::Zero(3,6), initVelCov, Eigen::MatrixXd::Zero(3,6),
			   Eigen::MatrixXd::Zero(6,9), initBiasCov;

    /////////// Choosed by trial and error ///////////////

    integrationPSD << 1e-8,   0,   0,
    					 0,1e-8,   0,
						 0,   0,1e-8; // error committed in integrating position from velocities, 1e-8 seems good



	// Rotation around y=-pi
	povTlidar_imu <<  1,  0,  0,  -1.26,
					  0,  1,  0,  0,
					  0,  0,  1,  -1.176,
					  0,  0,  0,  1;
	// Rotation around z=-pi/2
	povTlidar_ahrs <<  1,  0,  0,  -1.26,
			  	  	   0,  1,  0,  0,
					   0,  0,  1,  -1.176,
					   0,  0,  0,  1;

	sm_cov = 1e-2;

	sync_rate = 1;

	imu_rate = 50;

	nrow_scan = 16;
	ncol_scan = 1800;

	// ALSO, REMOVE
	// compass + AHRS constraint in synchronizer
	// MEKF_ahrsrollpitch_measurement_update in localizer
	// heading in position covariance in gps_nlet
	// inverse 9.81 in localizer
	// add -9.81 to p in synchronizer
	// change nrow_scan and ncol_scan
	// remove >100 for removing outlier points in scanmatcher
	// remove pitch_cal in scanmatcher


    /////////////////////////////////////////////////////////////////


    // static tf map2odom
    static tf2_ros::StaticTransformBroadcaster map2odom;
    geometry_msgs::TransformStamped static_transformStamped1;

    static_transformStamped1.header.stamp = ros::Time::now();
    static_transformStamped1.header.frame_id = "map";
    static_transformStamped1.child_frame_id = "odom";
    static_transformStamped1.transform.translation.x = 0;
	static_transformStamped1.transform.translation.y = 0;
	static_transformStamped1.transform.translation.z = 0;
	tf2::Quaternion quat;
	quat.setRPY(0,0,0);
	static_transformStamped1.transform.rotation.x = quat.x();
	static_transformStamped1.transform.rotation.y = quat.y();
	static_transformStamped1.transform.rotation.z = quat.z();
	static_transformStamped1.transform.rotation.w = quat.w();
	map2odom.sendTransform(static_transformStamped1);


    // static tf map2odom
    static tf2_ros::StaticTransformBroadcaster baselink2velodyne;
    geometry_msgs::TransformStamped static_transformStamped2;

    static_transformStamped2.header.stamp = ros::Time::now();
    static_transformStamped2.header.frame_id = "base_link";
    static_transformStamped2.child_frame_id = "velodyne";
    static_transformStamped2.transform.translation.x = 0;
	static_transformStamped2.transform.translation.y = 0;
	static_transformStamped2.transform.translation.z = 0;
	static_transformStamped2.transform.rotation.x = quat.x();
	static_transformStamped2.transform.rotation.y = quat.y();
	static_transformStamped2.transform.rotation.z = quat.z();
	static_transformStamped2.transform.rotation.w = quat.w();
	map2odom.sendTransform(static_transformStamped2);

}


Data_sglton *Data_sglton::get()
{
    if(!s_Instance)
        s_Instance = new Data_sglton;
    return s_Instance;
}    

// This function returns a Tp_q where q looks like x,y,z,roll,pitch,yaw from p point of view
Eigen::Matrix4d Data_sglton::POVtranslaEul2HomoMat(double x, double y, double z, double roll, double pitch, double yaw)
{
	Eigen::Matrix4d Tp_q;

	Tp_q << cos(yaw)*cos(pitch),	cos(yaw)*sin(pitch)*sin(roll)-sin(yaw)*cos(roll),	cos(yaw)*sin(pitch)*cos(roll)+sin(yaw)*sin(roll),	x,
 			sin(yaw)*cos(pitch),	sin(yaw)*sin(pitch)*sin(roll)+cos(yaw)*cos(roll),	sin(yaw)*sin(pitch)*cos(roll)-cos(yaw)*sin(roll),	y,
			-sin(pitch),			cos(pitch)*sin(roll),								cos(pitch)*cos(roll),								z,
			0,						0,													0,													1;

	return Tp_q;
}

// This function returns x,y,z,roll,pitch,yaw of p viewed from q
// see http://planning.cs.uiuc.edu/node103.html
void Data_sglton::POVhomoMat2TranslaEul(Eigen::Matrix4d Tp_q, double *x, double *y, double *z, double *roll, double *pitch, double *yaw)
{
	*x = Tp_q(0,3);
	*y = Tp_q(1,3);
	*z = Tp_q(2,3);
	*yaw = atan2(Tp_q(1,0),Tp_q(0,0));
	*pitch = atan2(-Tp_q(2,0), sqrt(pow(Tp_q(2,1),2)+pow(Tp_q(2,2),2)));
	*roll = atan2(Tp_q(2,1), Tp_q(2,2));
}

// See le ny notes
void Data_sglton::POVhomoMat2TranslaQuat(Eigen::Matrix4d Tp_q, double *x, double *y, double *z, double *qw, double *qx, double *qy, double *qz)
{
	*x = Tp_q(0,3);
	*y = Tp_q(1,3);
	*z = Tp_q(2,3);
	*qw = 0.5*sqrt((1+Tp_q(0,0)+Tp_q(1,1)+Tp_q(2,2) < 0) ? 0 : 1+Tp_q(0,0)+Tp_q(1,1)+Tp_q(2,2));
	*qx = 0.5*copysign(1.0,Tp_q(2,1)-Tp_q(1,2))*sqrt((1+Tp_q(0,0)-Tp_q(1,1)-Tp_q(2,2) < 0) ? 0 : 1+Tp_q(0,0)-Tp_q(1,1)-Tp_q(2,2));
	*qy = 0.5*copysign(1.0,Tp_q(0,2)-Tp_q(2,0))*sqrt((1-Tp_q(0,0)+Tp_q(1,1)-Tp_q(2,2) < 0) ? 0 : 1-Tp_q(0,0)+Tp_q(1,1)-Tp_q(2,2));
	*qz = 0.5*copysign(1.0,Tp_q(1,0)-Tp_q(0,1))*sqrt((1-Tp_q(0,0)-Tp_q(1,1)+Tp_q(2,2) < 0) ? 0 : 1-Tp_q(0,0)-Tp_q(1,1)+Tp_q(2,2));
}

Eigen::Matrix4d Data_sglton::POVTranslaQuat2homoMat(double x, double y, double z, double q0, double q1, double q2, double q3)
{
	//q0 = qw
	//q1 = qx
	//q2 = qy
	//q3 = qz

	Eigen::Matrix4d Tp_q;

	Tp_q << pow(q0,2)+pow(q1,2)-pow(q2,2)-pow(q3,2),	2*(q1*q2-q0*q3),	2*(q1*q3+q0*q2),	x,
			2*(q1*q2+q0*q3),	pow(q0,2)-pow(q1,2)+pow(q2,2)-pow(q3,2),	2*(q2*q3-q0*q1),	y,
			2*(q1*q3-q0*q2), 2*(q2*q3+q0*q1), pow(q0,2)-pow(q1,2)-pow(q2,2)+pow(q3,2),			z,
			0,			0,					0,													1;


	return Tp_q;
}


void Data_sglton::ZYXeul2Quat(double roll, double pitch, double yaw, double *w, double *x, double *y, double *z)
{
	*w = cos(roll/2)*cos(pitch/2)*cos(yaw/2)+sin(roll/2)*sin(pitch/2)*sin(yaw/2);
	*x = sin(roll/2)*cos(pitch/2)*cos(yaw/2)-cos(roll/2)*sin(pitch/2)*sin(yaw/2);
	*y = cos(roll/2)*sin(pitch/2)*cos(yaw/2)+sin(roll/2)*cos(pitch/2)*sin(yaw/2);
	*z = cos(roll/2)*cos(pitch/2)*sin(yaw/2)-sin(roll/2)*sin(pitch/2)*cos(yaw/2);
}

void Data_sglton::Quat2ZYXeul(double w, double x, double y, double z, double *roll, double *pitch, double *yaw)
{
	*roll = atan2(2*(w*x+y*z), 1-2*(pow(x,2)+pow(y,2)));
    *pitch = asin(2*(w*y-z*x));
    *yaw = atan2(2*(w*z+x*y), 1-2*(pow(y,2)+pow(z,2)));
    if(*yaw<0) *yaw=2*M_PI+*yaw;
}
