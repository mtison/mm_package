#include <loopcloser_nlet.h>

namespace nodelet_ns
{
    Loopcloser_nlet::Loopcloser_nlet(void)
    {
    }

    void Loopcloser_nlet::onInit(void)
    {
        ros::NodeHandle& n = getNodeHandle();

        data = Data_sglton::get();


        downSizeFilterICP.setLeafSize(0.4, 0.4, 0.4);


        timer = n.createTimer(ros::Duration(0.1), &Loopcloser_nlet::scancontextmatcher, this, true);


    	ROS_INFO("\033[1;32m----> loopcloser_nlet Started.\033[0m");

    }


    void Loopcloser_nlet::scancontextmatcher(const ros::TimerEvent& event)
    {

    	static int idx = 0;

    	while(ros::ok())
    	{
    		std::cout << "??????????EXIT loopcloser?????????" << std::endl;
    		/////////////////////////////////
    		data->loopcloser_barrier.lock();
    		////////////////////////////////
    		std::cout << "??????????ENTER loopcloser?????????" << std::endl;

			if(data->nodes_wptcl.empty())
				continue;

			local_last_node = *data->nodes_wptcl.back();


			data->node_mutex.lock();




	        ////////////////////////////////////////////////////
	        // First Stage: Obtaining best candidates from ring feature
	        ////////////////////////////////////////////////////

			std::vector<std::vector<float>> tmp;
			int i=0;
			while(local_last_node.time - data->nodes_wptcl[i]->time > 30) //don't check the ptcl obtained since the last 30 seconds
			{
				tmp.push_back(data->nodes_wptcl[i]->ringkey);
				i++;
			}
			if(tmp.empty())
			{
				data->node_mutex.unlock();
				continue;
			}

			polarcontext_tree_ = std::make_unique<KDTreeVectorOfVectorsAdaptor<std::vector<std::vector<float>>, float>>(data->Nr , tmp, 10);

	        std::vector<unsigned long int> candidate_indexes(data->first_stage_ncandidates );
	        std::vector<float> dummy(data->first_stage_ncandidates );

	        nanoflann::KNNResultSet<float> knnsearch_result(data->first_stage_ncandidates );
	        knnsearch_result.init( &candidate_indexes[0], &dummy[0] );
	        polarcontext_tree_->index->findNeighbors( knnsearch_result, &local_last_node.ringkey[0] , nanoflann::SearchParams(10) );


	        ////////////////////////////////////////////////////
	        // Second Stage: Obtaining the best candidate from scan-context feature
	        ////////////////////////////////////////////////////

	        double min_dist = 10000000; // init with somthing large
	        float yawDiffRad;
	        int candidate_id = -1; // init with -1, -1 means no loop (== LeGO-LOAM's variable "closestHistoryFrameID")
	        for ( int i = 0; i < data->first_stage_ncandidates; i++ )
	        {
	            std::pair<double, int> sc_dist_result = distanceBtnScanContext( local_last_node.sc, data->nodes_wptcl[candidate_indexes[i]]->sc);

	            if(sc_dist_result.first < min_dist &&  sc_dist_result.first < 0.3)
	            {
	                min_dist = sc_dist_result.first;
	                yawDiffRad = (sc_dist_result.second * 360.0 / double(data->Ns))*M_PI/180;
	                candidate_id = data->nodes_wptcl[candidate_indexes[i]]->id;
	            }
	        }

			if( candidate_id == -1 /* No loop found */)
			{
				data->node_mutex.unlock();
				continue;
			}



			std::cout << "------------------- SC loop found! between " << local_last_node.id << " and " << candidate_id << "." << std::endl; // giseop


	        ////////////////////////////////////////////////////
	        // Third Stage: Is the best candidate really a loopclosure
	        ////////////////////////////////////////////////////

			// extract cloud
			pcl::PointCloud<pcl::PointXYZI>::Ptr lastKeyframeCloud(new pcl::PointCloud<pcl::PointXYZI>());
			pcl::PointCloud<pcl::PointXYZI>::Ptr candidateKeyframeCloud(new pcl::PointCloud<pcl::PointXYZI>());

			pcl::PointCloud<pcl::PointXYZI>::Ptr tmp1(new pcl::PointCloud<pcl::PointXYZI>());
			pcl::PointCloud<pcl::PointXYZI>::Ptr tmp2(new pcl::PointCloud<pcl::PointXYZI>());

			pcl::transformPointCloud(*local_last_node.cornerptcl, *tmp1, local_last_node.Tw_r);
			*lastKeyframeCloud = *tmp1;

			pcl::transformPointCloud(*local_last_node.surfptcl, *tmp2, local_last_node.Tw_r);
			*lastKeyframeCloud += *tmp2;

			for (int i = -25; i <= 25; ++i)
			{
				int keyNear = candidate_id + i;
				if (keyNear < 0 || keyNear >= data->n_node )
					continue;
				if(!data->nodes[keyNear]->has_ptcl)
					continue;

				pcl::PointCloud<pcl::PointXYZI>::Ptr tmp3(new pcl::PointCloud<pcl::PointXYZI>());
				pcl::PointCloud<pcl::PointXYZI>::Ptr tmp4(new pcl::PointCloud<pcl::PointXYZI>());

				pcl::transformPointCloud(*data->nodes[keyNear]->cornerptcl, *tmp3, data->nodes[keyNear]->Tw_r);
				*candidateKeyframeCloud += *tmp3;

				pcl::transformPointCloud(*data->nodes[keyNear]->surfptcl, *tmp4, data->nodes[keyNear]->Tw_r);
				*candidateKeyframeCloud += *tmp4;

			}



			data->node_mutex.unlock();


			// downsample near keyframes
			pcl::PointCloud<pcl::PointXYZI>::Ptr tmp5(new pcl::PointCloud<pcl::PointXYZI>());
			downSizeFilterICP.setInputCloud(lastKeyframeCloud);
			downSizeFilterICP.filter(*tmp5);
			*lastKeyframeCloud = *tmp5;
			// downsample near keyframes
			pcl::PointCloud<pcl::PointXYZI>::Ptr tmp6(new pcl::PointCloud<pcl::PointXYZI>());
			downSizeFilterICP.setInputCloud(candidateKeyframeCloud);
			downSizeFilterICP.filter(*tmp6);
			*candidateKeyframeCloud = *tmp6;


			// ICP Settings
			static pcl::IterativeClosestPoint<pcl::PointXYZI, pcl::PointXYZI> icp;
			icp.setMaxCorrespondenceDistance(150); // giseop , use a value can cover 2*historyKeyframeSearchNum range in meter
			icp.setMaximumIterations(100);
			icp.setTransformationEpsilon(1e-6);
			icp.setEuclideanFitnessEpsilon(1e-6);
			icp.setRANSACIterations(0);

			// Align clouds
			icp.setInputSource(lastKeyframeCloud);
			icp.setInputTarget(candidateKeyframeCloud);
			pcl::PointCloud<pcl::PointXYZI>::Ptr unused_result(new pcl::PointCloud<pcl::PointXYZI>());

			// init solution
			Eigen::Matrix4d Tcuri_prei = data->POVtranslaEul2HomoMat(0,0,0,0,0,yawDiffRad);
			Eigen::Matrix4d Tinit = data->nodes[candidate_id]->Tw_r * Tcuri_prei.inverse() * local_last_node.Tw_r.inverse();

			icp.align(*unused_result,Tinit.cast<float>());


			if (icp.hasConverged() == false || icp.getFitnessScore() > 0.15) {
				std::cout << "ICP fitness test failed (" << icp.getFitnessScore() << " > " << 0.15 << "). Reject this SC loop." << std::endl;
				continue;
			} else {
				std::cout << "ICP fitness test passed (" << icp.getFitnessScore() << " < " << 0.15 << "). Add this SC loop." << std::endl;
			}
	//		pcl::PointCloud<pcl::PointXYZI>::Ptr moved(new pcl::PointCloud<pcl::PointXYZI>());
	//		pcl::PointCloud<pcl::PointXYZI>::Ptr mixed(new pcl::PointCloud<pcl::PointXYZI>());
	//		pcl::transformPointCloud (*cureKeyframeCloud, *moved, icp.getFinalTransformation());
	//		*mixed = *prevKeyframeCloud;
	//		*mixed += *moved;
	//		std::cout << "idx = " << idx << std::endl;
	//		std::cout << "fitness = " << icp.getFitnessScore() << std::endl;
	//		std::cout << "matrix" << std::endl << icp.getFinalTransformation() << std::endl;
	//		pcl::io::savePCDFileASCII ("cur" + std::to_string(idx) + ".pcd", *cureKeyframeCloud);
	//		pcl::io::savePCDFileASCII ("prev" + std::to_string(idx) + ".pcd", *prevKeyframeCloud);
	//		pcl::io::savePCDFileASCII ("moved" + std::to_string(idx) + ".pcd", *moved);
	//		pcl::io::savePCDFileASCII ("mixed" + std::to_string(idx) + ".pcd", *mixed);
	//
	//
	//		idx++;



			// Get pose transformation
			Eigen::MatrixXd Tw_cur = icp.getFinalTransformation().cast<double>() * local_last_node.Tw_r;
			Eigen::MatrixXd Tcur_pre = Tw_cur.inverse() * data->nodes[candidate_id]->Tw_r;

			std::cout << "matrix" << std::endl << Tcur_pre << std::endl;
			std::cout << "yaw = " << atan2(Tcur_pre(1,0),Tcur_pre(0,0)) << std::endl;
			std::cout << "yawdiff = " << yawDiffRad << std::endl;
			gtsam::Vector Vector6(6);
			float noiseScore = icp.getFitnessScore();
			Vector6 << noiseScore, noiseScore, noiseScore, noiseScore, noiseScore, noiseScore;
	//		Vector6 << 1e-6, 1e-6, 1e-6, 1e-6, 1e-6, 1e-6;
			gtsam::noiseModel::Diagonal::shared_ptr constraintNoise = gtsam::noiseModel::Diagonal::Variances(Vector6);

			gtsam::BetweenFactor<gtsam::Pose3> loopclosureFactor(P(local_last_node.id), P(candidate_id), gtsam::Pose3(Tcur_pre), constraintNoise);
			data->gtSAMgraph.add(loopclosureFactor);


    	}



    }





    std::pair<double, int> Loopcloser_nlet::distanceBtnScanContext( Eigen::MatrixXd &_sc1, Eigen::MatrixXd &_sc2 )
    {
        // 1. fast align using variant key (not in original IROS18)
        Eigen::MatrixXd vkey_sc1 = createSectorkeyfeature( _sc1 );
        Eigen::MatrixXd vkey_sc2 = createSectorkeyfeature( _sc2 );
        int argmin_vkey_shift = fastAlignUsingVkey( vkey_sc1, vkey_sc2 );

        const int SEARCH_RADIUS = round( 0.5 * 0.1 * _sc1.cols() ); // a half of search range
        std::vector<int> shift_idx_search_space { argmin_vkey_shift };
        for ( int ii = 1; ii < SEARCH_RADIUS + 1; ii++ )
        {
            shift_idx_search_space.push_back( (argmin_vkey_shift + ii + _sc1.cols()) % _sc1.cols() );
            shift_idx_search_space.push_back( (argmin_vkey_shift - ii + _sc1.cols()) % _sc1.cols() );
        }
        std::sort(shift_idx_search_space.begin(), shift_idx_search_space.end());

        // 2. fast columnwise diff
        int argmin_shift = 0;
        double min_sc_dist = 10000000;
        for ( int num_shift: shift_idx_search_space )
        {
            Eigen::MatrixXd sc2_shifted = circshift(_sc2, num_shift);
            double cur_sc_dist = distDirectSC( _sc1, sc2_shifted );
            if( cur_sc_dist < min_sc_dist )
            {
                argmin_shift = num_shift;
                min_sc_dist = cur_sc_dist;
            }
        }

        return std::make_pair(min_sc_dist, argmin_shift);

    } // distanceBtnScanContext


    int Loopcloser_nlet::fastAlignUsingVkey( Eigen::MatrixXd & _vkey1, Eigen::MatrixXd & _vkey2)
    {
        int argmin_vkey_shift = 0;
        double min_veky_diff_norm = 10000000;
        for ( int shift_idx = 0; shift_idx < _vkey1.cols(); shift_idx++ )
        {
            Eigen::MatrixXd vkey2_shifted = circshift(_vkey2, shift_idx);

            Eigen::MatrixXd vkey_diff = _vkey1 - vkey2_shifted;

            double cur_diff_norm = vkey_diff.norm();
            if( cur_diff_norm < min_veky_diff_norm )
            {
                argmin_vkey_shift = shift_idx;
                min_veky_diff_norm = cur_diff_norm;
            }
        }

        return argmin_vkey_shift;

    } // fastAlignUsingVkey


    Eigen::MatrixXd Loopcloser_nlet::circshift( Eigen::MatrixXd &_mat, int _num_shift )
    {
        // shift columns to right direction
        assert(_num_shift >= 0);

        if( _num_shift == 0 )
        {
            Eigen::MatrixXd shifted_mat( _mat );
            return shifted_mat; // Early return
        }

        Eigen::MatrixXd shifted_mat = Eigen::MatrixXd::Zero( _mat.rows(), _mat.cols() );
        for ( int col_idx = 0; col_idx < _mat.cols(); col_idx++ )
        {
            int new_location = (col_idx + _num_shift) % _mat.cols();
            shifted_mat.col(new_location) = _mat.col(col_idx);
        }

        return shifted_mat;

    } // circshift

    double Loopcloser_nlet::distDirectSC ( Eigen::MatrixXd &_sc1, Eigen::MatrixXd &_sc2 )
    {
        int num_eff_cols = 0; // i.e., to exclude all-nonzero sector
        double sum_sector_similarity = 0;
        for ( int col_idx = 0; col_idx < _sc1.cols(); col_idx++ )
        {
            Eigen::VectorXd col_sc1 = _sc1.col(col_idx);
            Eigen::VectorXd col_sc2 = _sc2.col(col_idx);

            if( (col_sc1.norm() == 0) | (col_sc2.norm() == 0) )
                continue; // don't count this sector pair.

            double sector_similarity = col_sc1.dot(col_sc2) / (col_sc1.norm() * col_sc2.norm());

            sum_sector_similarity = sum_sector_similarity + sector_similarity;
            num_eff_cols = num_eff_cols + 1;
        }

        double sc_sim = sum_sector_similarity / num_eff_cols;
        return 1.0 - sc_sim;

    } // distDirectSC

    Eigen::MatrixXd Loopcloser_nlet::createSectorkeyfeature( Eigen::MatrixXd &_desc )
    {
        /*
         * summary: columnwise mean vector
        */
        Eigen::MatrixXd variant_key(1, _desc.cols());
        for ( int col_idx = 0; col_idx < _desc.cols(); col_idx++ )
        {
            Eigen::MatrixXd curr_col = _desc.col(col_idx);
            variant_key(0, col_idx) = curr_col.mean();
        }

        return variant_key;
    } // SCManager::makeSectorkeyFromScancontext

    PLUGINLIB_EXPORT_CLASS(nodelet_ns::Loopcloser_nlet, nodelet::Nodelet);
}

