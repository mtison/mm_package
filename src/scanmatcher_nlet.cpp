#include "scanmatcher_nlet.h"


namespace nodelet_ns
{


	Scanmatcher_nlet::Scanmatcher_nlet(void)
	{
	}


	void Scanmatcher_nlet::onInit()
	{
		ros::NodeHandle& n = getNodeHandle();


		data = Data_sglton::get();


    	/////////////////////////////////////// IMAGEPROJECTION /////////////////////////////////////////

        subLaserCloud = n.subscribe<sensor_msgs::PointCloud2>("points_rawz", 1, &Scanmatcher_nlet::cloudHandler, this, ros::TransportHints().tcpNoDelay());


        pubExtractedCloud = n.advertise<sensor_msgs::PointCloud2> ("lio_sam/deskew/cloud_deskewed", 1);


        ptcl_pcl.reset(new pcl::PointCloud<PointXYZIRT>());
        extractedCloud.reset(new pcl::PointCloud<pcl::PointXYZI>());
        extractedCloudDS.reset(new pcl::PointCloud<pcl::PointXYZI>()); // giseop


        pointRangeVec.assign(data->nrow_scan*data->ncol_scan, 0); //N_SCAN*Horizon_SCAN
        pointColIdxVec.assign(data->nrow_scan*data->ncol_scan, 0); //N_SCAN*Horizon_SCAN
        startRingIdxVec.assign(data->nrow_scan, 0); //N_SCAN
        endRingIdxVec.assign(data->nrow_scan, 0); //N_SCAN


        pcl::console::setVerbosityLevel(pcl::console::L_ERROR);

    	/////////////////////////////////////////////////////////////////////////////////////////////////





    	////////////////////////////////////// FEATUREEXTRACTION ////////////////////////////////////////


        pubCornerPoints = n.advertise<sensor_msgs::PointCloud2>("lio_sam/feature/cloud_corner", 1);
        pubSurfacePoints = n.advertise<sensor_msgs::PointCloud2>("lio_sam/feature/cloud_surface", 1);



        // initializationValue
        cloudSmoothness.resize(data->nrow_scan*data->ncol_scan); //N_SCAN*Horizon_SCAN

        downSizeFilterFeature.setLeafSize(0.2, 0.2, 0.2); //odometrySurfLeafSize=0.4


        cloudCurvature = new float[data->nrow_scan*data->ncol_scan]; //N_SCAN*Horizon_SCAN
        cloudNeighborPicked = new int[data->nrow_scan*data->ncol_scan]; //N_SCAN*Horizon_SCAN
        cloudLabel = new int[data->nrow_scan*data->ncol_scan]; //N_SCAN*Horizon_SCAN

    	/////////////////////////////////////////////////////////////////////////////////////////////////


    	/////////////////////////////////////// MAPOPTIMIZATION /////////////////////////////////////////
		local_n_node = 0;
		local_n_node_opt = 0;


		downSizeFilter.setLeafSize(0.1, 0.1, 0.1); //mappingCornerLeafSize=0.2

		cornerCloud.reset(new pcl::PointCloud<pcl::PointXYZI>()); // corner feature set from odoOptimization
		surfaceCloud.reset(new pcl::PointCloud<pcl::PointXYZI>()); // surf feature set from odoOptimization
		cornerCloudDS.reset(new pcl::PointCloud<pcl::PointXYZI>()); // downsampled corner featuer set from odoOptimization
		surfaceCloudDS.reset(new pcl::PointCloud<pcl::PointXYZI>()); // downsampled surf featuer set from odoOptimization

		downSizeFilterSC.setLeafSize(0.5, 0.5, 0.5); // giseop


		laserCloudOri.reset(new pcl::PointCloud<pcl::PointXYZI>());
		coeffSel.reset(new pcl::PointCloud<pcl::PointXYZI>());

		laserCloudOriCornerVec.resize(data->nrow_scan * data->ncol_scan); //N_SCAN * Horizon_SCAN
		coeffSelCornerVec.resize(data->nrow_scan * data->ncol_scan);
		laserCloudOriCornerFlag.resize(data->nrow_scan * data->ncol_scan);
		laserCloudOriSurfVec.resize(data->nrow_scan * data->ncol_scan);
		coeffSelSurfVec.resize(data->nrow_scan * data->ncol_scan);
		laserCloudOriSurfFlag.resize(data->nrow_scan * data->ncol_scan);

		std::fill(laserCloudOriCornerFlag.begin(), laserCloudOriCornerFlag.end(), false);
		std::fill(laserCloudOriSurfFlag.begin(), laserCloudOriSurfFlag.end(), false);


		local_lastcorner_transformed.reset(new pcl::PointCloud<pcl::PointXYZI>());
		local_lastsurf_transformed.reset(new pcl::PointCloud<pcl::PointXYZI>());
		local_lastcorner_transformedDS.reset(new pcl::PointCloud<pcl::PointXYZI>());
		local_lastsurf_transformedDS.reset(new pcl::PointCloud<pcl::PointXYZI>());

		kdtreeCornerFromMap.reset(new pcl::KdTreeFLANN<pcl::PointXYZI>());
		kdtreeSurfFromMap.reset(new pcl::KdTreeFLANN<pcl::PointXYZI>());

		for (int i = 0; i < 6; ++i){
			transformTobeMapped[i] = 0;
		}

		matP = cv::Mat(6, 6, CV_32F, cv::Scalar::all(0));

		isDegenerate = false;
    	/////////////////////////////////////////////////////////////////////////////////////////////////





		ROS_INFO("\033[1;32m----> scanmatcher_nlet Started.\033[0m");

	}






	// We enter this function at around 10hz (lidar rate)
	void Scanmatcher_nlet::cloudHandler(const sensor_msgs::PointCloud2ConstPtr& ptcl_ros_ptr)
	{
		std::cout << "----------ENTER scanmatcher---------" << std::endl;


		ptcl_ros = std::move(*ptcl_ros_ptr); // to move the variable from a sensor_msgs::PointCloud2ConstPtr& to a sensor_msgs::PointCloud2
    	ptcl_pcl->clear();
		pcl::moveFromROSMsg(ptcl_ros, *ptcl_pcl); // to obtain a pcl::PointCloud from a sensor_msgs::PointCloud2

//		Eigen::Matrix4d pitch_cal;
//		pitch_cal << 0.9986295,  0.0000000, -0.0523360, 0,
//		   	   	     0.0000000,  1.0000000,  0.0000000, 0,
//		   	   	     0.0523360,  0.0000000,  0.9986295, 0,
//					 0,			 0,		 	 0,			1;
//		pcl::transformPointCloud (*ptcl_pcl, *ptcl_pcl, pitch_cal);

		// From here, we have two equivalent point cloud: ptcl_ros & ptcl_pcl

		projectandExtractPointCloud();


		// Publish on /lio_sam/deskew/cloud_info (for rviz visualization)
		sensor_msgs::PointCloud2 tmp1;
		pcl::toROSMsg(*extractedCloud, tmp1);
		tmp1.header = ptcl_ros.header;
		tmp1.header.frame_id = "base_link";
		pubExtractedCloud.publish(tmp1);


		extractedCloudDS->clear();
        downSizeFilterSC.setInputCloud(extractedCloud);
        downSizeFilterSC.filter(*extractedCloudDS);


        // HERE: scancontext features are created everytime a new pointcloud comes in. We are extracting features using a downsampled cloud_deskewed ptcl from imageProjection.cpp
        // THEN, the scancontext features are saved in polarcontexts variables
        sc_local = createScancontextfeature(*extractedCloudDS); // v1
        ringkey_local = createRingkeyfeature( sc_local );


        extractFeatures();


		sensor_msgs::PointCloud2 tmp2;
		pcl::toROSMsg(*cornerCloud, tmp2);
		tmp2.header = ptcl_ros.header;
		tmp2.header.frame_id = "base_link";
        pubCornerPoints.publish(tmp2);

		sensor_msgs::PointCloud2 tmp3;
		pcl::toROSMsg(*surfaceCloud, tmp3);
		tmp3.header = ptcl_ros.header;
		tmp3.header.frame_id = "base_link";
        pubSurfacePoints.publish(tmp3);


		data->node_mutex.lock();
		if(data->sync_flag)
		{
			data->sync_flag = false;
			local_n_node = data->n_node;

		}
		// If an optimization ended
		if(data->opt_flag)
		{
			local_n_node_opt = data->n_node_opt;
			std::cout << "OPT_FLAG FALSE" << std::endl;
			data->opt_flag = false;

			local_map_nodevec = data->localMapNodeVec;
			*local_lastcorner_transformed = *data->localMapCorner;
			*local_lastsurf_transformed = *data->localMapSurf;

		}
		data->node_mutex.unlock();

		std::cout << "map size = " << local_map_nodevec.size() << std::endl;

		if (local_map_nodevec.empty() || //  condition #1 is when the local map was not consistent, then optimizer had to reset it,
		    ptcl_ros_ptr->header.stamp.toSec()-local_map_nodevec.back().time > 5 || // condition #2 is if the map is consistent, but too far in time
			data->localizerQueue4sm.empty())
		{
			std::cout << "/////////////////////sheesh///////////////////// " << std::endl;
			// Downsample the current corner point cloud
			cornerCloudDS->clear();
			downSizeFilter.setInputCloud(cornerCloud);
			downSizeFilter.filter(*cornerCloudDS);

			// Downsample the current surface point cloud
			surfaceCloudDS->clear();
			downSizeFilter.setInputCloud(surfaceCloud);
			downSizeFilter.filter(*surfaceCloudDS);


			ptclconstraint ptcltmp;
			ptcltmp.time = ptcl_ros.header.stamp.toSec();
			ptcltmp.id = local_n_node;
			ptcltmp.cornerptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
			ptcltmp.surfptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
			pcl::copyPointCloud(*cornerCloudDS,  *ptcltmp.cornerptcl);// Copy this way since the point cloud vector needs a ptr
			pcl::copyPointCloud(*surfaceCloudDS,    *ptcltmp.surfptcl);
			ptcltmp.sc  = sc_local;
			ptcltmp.ringkey = ringkey_local;
			data->ptclconstraintQueue.push_back(ptcltmp);


		}
		else
		{
			Eigen::VectorXd tmp;
			do
			{
				tmp = data->localizerQueue4sm.front().state;
				data->localizerQueue4sm.pop_front();
			}
			while(!data->localizerQueue4sm.empty() && data->localizerQueue4sm.front().header.stamp.toSec() <= ptcl_ros.header.stamp.toSec());
			povImuPre_Tw_r = data->POVTranslaQuat2homoMat(tmp(0), tmp(1), tmp(2), tmp(6), tmp(7), tmp(8), tmp(9));


			// Downsample the previous transformed corner keyframe
			downSizeFilter.setInputCloud(local_lastcorner_transformed);
			downSizeFilter.filter(*local_lastcorner_transformedDS);
			// Downsample the previous transformed surface keyframe
			downSizeFilter.setInputCloud(local_lastsurf_transformed);
			downSizeFilter.filter(*local_lastsurf_transformedDS);


			// Downsample the current corner point cloud
			cornerCloudDS->clear();
			downSizeFilter.setInputCloud(cornerCloud);
			downSizeFilter.filter(*cornerCloudDS);

			// Downsample the current surface point cloud
			surfaceCloudDS->clear();
			downSizeFilter.setInputCloud(surfaceCloud);
			downSizeFilter.filter(*surfaceCloudDS);


			data->POVhomoMat2TranslaEul(povImuPre_Tw_r, &transformTobeMapped[3], &transformTobeMapped[4], &transformTobeMapped[5], &transformTobeMapped[0], &transformTobeMapped[1], &transformTobeMapped[2]);
			scan2MapOptimization();
			povSM_Tw_r = data->POVtranslaEul2HomoMat(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]);
			povSM_Trprev_r = local_map_nodevec.back().Tw_r.inverse() * povSM_Tw_r;

			smconstraint smtmp;
			smtmp.relT = gtsam::Pose3(povSM_Trprev_r);
			smtmp.relnoise = gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << data->sm_cov, data->sm_cov, data->sm_cov, data->sm_cov, data->sm_cov, data->sm_cov).finished());
			smtmp.time = ptcl_ros.header.stamp.toSec();
			smtmp.id = local_n_node;
			smtmp.previd = local_map_nodevec.back().id;
			smtmp.cornerptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
			smtmp.surfptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
			pcl::copyPointCloud(*cornerCloudDS,  *smtmp.cornerptcl);// Copy this way since the point cloud vector needs a ptr
			pcl::copyPointCloud(*surfaceCloudDS,    *smtmp.surfptcl);
			smtmp.sc  = sc_local;
			smtmp.ringkey = ringkey_local;


			data->smconstraintQueue.push_back(smtmp);


			ptclconstraint ptcltmp;
			ptcltmp.time = ptcl_ros.header.stamp.toSec();
			ptcltmp.id = local_n_node;
			ptcltmp.cornerptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
			ptcltmp.surfptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
			pcl::copyPointCloud(*cornerCloudDS,  *ptcltmp.cornerptcl);// Copy this way since the point cloud vector needs a ptr
			pcl::copyPointCloud(*surfaceCloudDS,    *ptcltmp.surfptcl);
			ptcltmp.sc  = sc_local;
			ptcltmp.ringkey = ringkey_local;
			data->ptclconstraintQueue.push_back(ptcltmp);

		}


		std::cout << "----------EXIT scanmatcher---------" << std::endl;

	}











	///////////////////////////////////////////// IMAGEPROJECTION //////////////////////////////////////////
	/////////////////////////////////////////////////// #3 /////////////////////////////////////////////////
	void Scanmatcher_nlet::projectandExtractPointCloud(void)
	{
		int nb_points = 0;
        // The image is data->nrow_scan,data->ncol_scan elements of type CV_32F and we fill it with FLT_MAX value
        rangeImage = cv::Mat(data->nrow_scan, data->ncol_scan, CV_32F, cv::Scalar::all(FLT_MAX)); //N_SCAN, Horizon_SCAN,
	    extractedCloud->clear();

		// range image projection
		for (int i = 0; i < ptcl_pcl->points.size(); ++i)
		{
			float horizonAngle = atan2(ptcl_pcl->points[i].x, ptcl_pcl->points[i].y) * 180 / M_PI; // Angle
			float ang_res_x = 360.0/float(data->ncol_scan); //Horizon_SCAN=1800

			int rowIdn = ptcl_pcl->points[i].ring;
			int columnIdn = -round((horizonAngle-90.0)/ang_res_x) + data->ncol_scan/2; //Horizon_SCAN=1800
			if (columnIdn >= data->ncol_scan) columnIdn -= data->ncol_scan; //Horizon_SCAN
			float range = sqrt(pow(ptcl_pcl->points[i].x,2) + pow(ptcl_pcl->points[i].y,2) + pow(ptcl_pcl->points[i].z,2));


			// We omit egovehicle points
			if (range < 1.0 || range > 110)
				continue;




			// Pixel already filled with another point projection are removed
			if (rangeImage.at<float>(rowIdn, columnIdn) != FLT_MAX)
				continue;

			///////////////// FILL RANGE IMAGE /////////////////
			rangeImage.at<float>(rowIdn, columnIdn) = range; // rowIdn = [0-15] & columnIdn = [0-1799]

			///////////////// ALSO ADD THOSE POINTS IN A NEW POINTCLOUD /////////////////
			pcl::PointXYZI tmp;
			tmp.x = ptcl_pcl->points[i].x; //Horizon_SCAN=1800
			tmp.y = ptcl_pcl->points[i].y;
			tmp.z = ptcl_pcl->points[i].z;
			tmp.intensity = std::min(int(ptcl_pcl->points[i].intensity), 100); // min at 100 for visualization color in rviz
			extractedCloud->push_back(tmp);
		}

		// From here, - points that fitted in the same pixel(doublons) got removed
		//            - egovehicle points got removed


		////////////// I HAVE NO IDEA WHAT IS GOING ON HERE ///////////////
		int count = 0;
		// extract segmented cloud for lidar odometry
		for (int i = 0; i < data->nrow_scan; ++i) //N_SCAN = 16
		{
			startRingIdxVec[i] = count - 1 + 5;

			for (int j = 0; j < data->ncol_scan; ++j) //Horizon_SCAN
			{
				if (rangeImage.at<float>(i,j) != FLT_MAX)
				{
					// mark the points' column index for marking occlusion later
					pointColIdxVec[count] = j;
					// save range info
					pointRangeVec[count] = rangeImage.at<float>(i,j);
					// size of extracted cloud
					++count;
				}
			}
			endRingIdxVec[i] = count -1 - 5;
		}
	}






	////////////////////////////////////////////////////////////////////////////////////////////////////////






















	//////////////////////////////////////////// FEATUREEXTRACTION /////////////////////////////////////////
    /////////////////////////////////////////////////// #3 /////////////////////////////////////////////////
    void Scanmatcher_nlet::extractFeatures(void)
    {

    	//calculateSmoothness
        for (int i = 5; i < extractedCloud->points.size()-5; i++)
        {
            float diffRange = pointRangeVec[i-5] + pointRangeVec[i-4]
                            + pointRangeVec[i-3] + pointRangeVec[i-2]
                            + pointRangeVec[i-1] - pointRangeVec[i] * 10
                            + pointRangeVec[i+1] + pointRangeVec[i+2]
                            + pointRangeVec[i+3] + pointRangeVec[i+4]
                            + pointRangeVec[i+5];

            cloudCurvature[i] = diffRange*diffRange;//diffX * diffX + diffY * diffY + diffZ * diffZ;

            cloudNeighborPicked[i] = 0;
            cloudLabel[i] = 0;
            // cloudSmoothness for sorting
            cloudSmoothness[i].value = cloudCurvature[i];
            cloudSmoothness[i].ind = i;
        }



        // mark occluded points and parallel beam points
        for (int i = 5; i < extractedCloud->points.size()-6; ++i)
        {
            // occluded points
            float depth1 = pointRangeVec[i];
            float depth2 = pointRangeVec[i+1];
            int columnDiff = std::abs(int(pointColIdxVec[i+1] - pointColIdxVec[i]));

            if (columnDiff < 10){
                // 10 pixel diff in range image
                if (depth1 - depth2 > 0.3){
                    cloudNeighborPicked[i - 5] = 1;
                    cloudNeighborPicked[i - 4] = 1;
                    cloudNeighborPicked[i - 3] = 1;
                    cloudNeighborPicked[i - 2] = 1;
                    cloudNeighborPicked[i - 1] = 1;
                    cloudNeighborPicked[i] = 1;
                }else if (depth2 - depth1 > 0.3){
                    cloudNeighborPicked[i + 1] = 1;
                    cloudNeighborPicked[i + 2] = 1;
                    cloudNeighborPicked[i + 3] = 1;
                    cloudNeighborPicked[i + 4] = 1;
                    cloudNeighborPicked[i + 5] = 1;
                    cloudNeighborPicked[i + 6] = 1;
                }
            }
            // parallel beam
            float diff1 = std::abs(float(pointRangeVec[i-1] - pointRangeVec[i]));
            float diff2 = std::abs(float(pointRangeVec[i+1] - pointRangeVec[i]));

            if (diff1 > 0.02 * pointRangeVec[i] && diff2 > 0.02 * pointRangeVec[i])
                cloudNeighborPicked[i] = 1;
        }



        cornerCloud->clear();
        surfaceCloud->clear();

        pcl::PointCloud<pcl::PointXYZI>::Ptr surfaceCloudScan(new pcl::PointCloud<pcl::PointXYZI>());
        pcl::PointCloud<pcl::PointXYZI>::Ptr surfaceCloudScanDS(new pcl::PointCloud<pcl::PointXYZI>());

        for (int i = 0; i < data->nrow_scan; i++) //N_SCAN = data->nrow_scan
        {
            surfaceCloudScan->clear();

            for (int j = 0; j < 6; j++)
            {

                int sp = (startRingIdxVec[i] * (6 - j) + endRingIdxVec[i] * j) / 6;
                int ep = (startRingIdxVec[i] * (5 - j) + endRingIdxVec[i] * (j + 1)) / 6 - 1;

                if (sp >= ep)
                    continue;

                std::sort(cloudSmoothness.begin()+sp, cloudSmoothness.begin()+ep, by_value());

                int largestPickedNum = 0;
                for (int k = ep; k >= sp; k--)
                {
                    int ind = cloudSmoothness[k].ind;
                    if (cloudNeighborPicked[ind] == 0 && cloudCurvature[ind] > 1.0) //edgeThreshold = 1.0
                    {
                        largestPickedNum++;
                        if (largestPickedNum <= 20){
                            cloudLabel[ind] = 1;
                            cornerCloud->push_back(extractedCloud->points[ind]);
                        } else {
                            break;
                        }

                        cloudNeighborPicked[ind] = 1;
                        for (int l = 1; l <= 5; l++)
                        {
                            int columnDiff = std::abs(int(pointColIdxVec[ind + l] - pointColIdxVec[ind + l - 1]));
                            if (columnDiff > 10)
                                break;
                            cloudNeighborPicked[ind + l] = 1;
                        }
                        for (int l = -1; l >= -5; l--)
                        {
                            int columnDiff = std::abs(int(pointColIdxVec[ind + l] - pointColIdxVec[ind + l + 1]));
                            if (columnDiff > 10)
                                break;
                            cloudNeighborPicked[ind + l] = 1;
                        }
                    }
                }

                for (int k = sp; k <= ep; k++)
                {
                    int ind = cloudSmoothness[k].ind;
                    if (cloudNeighborPicked[ind] == 0 && cloudCurvature[ind] < 0.1) // surfThreshold = 0.1
                    {

                        cloudLabel[ind] = -1;
                        cloudNeighborPicked[ind] = 1;

                        for (int l = 1; l <= 5; l++) {

                            int columnDiff = std::abs(int(pointColIdxVec[ind + l] - pointColIdxVec[ind + l - 1]));
                            if (columnDiff > 10)
                                break;

                            cloudNeighborPicked[ind + l] = 1;
                        }
                        for (int l = -1; l >= -5; l--) {

                            int columnDiff = std::abs(int(pointColIdxVec[ind + l] - pointColIdxVec[ind + l + 1]));
                            if (columnDiff > 10)
                                break;

                            cloudNeighborPicked[ind + l] = 1;
                        }
                    }
                }

                for (int k = sp; k <= ep; k++)
                {
                    if (cloudLabel[k] <= 0){
                        surfaceCloudScan->push_back(extractedCloud->points[k]);
                    }
                }
            }

            surfaceCloudScanDS->clear();
            downSizeFilterFeature.setInputCloud(surfaceCloudScan);
            downSizeFilterFeature.filter(*surfaceCloudScanDS);

            *surfaceCloud += *surfaceCloudScanDS;
        }

        startRingIdxVec.clear();
        endRingIdxVec.clear();
        pointColIdxVec.clear();
        pointRangeVec.clear();
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////










	//////////////////////////////////// MAPOPTIMIZATION /////////////////////////////////////
	////////////////////////////////////////// #4 ////////////////////////////////////////////
	void Scanmatcher_nlet::scan2MapOptimization(void)
	{
		kdtreeCornerFromMap->setInputCloud(local_lastcorner_transformedDS);
		kdtreeSurfFromMap->setInputCloud(local_lastsurf_transformedDS);

		for (int iterCount = 0; iterCount < 30; iterCount++)
		{
			laserCloudOri->clear();
			coeffSel->clear();

			cornerOptimization();
			surfOptimization();

			combineOptimizationCoeffs();

			if (LMOptimization(iterCount) == true)
				break;
		}

	}

	void Scanmatcher_nlet::cornerOptimization(void)
	{
		transPointAssociateToMap = pcl::getTransformation(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]); // trans 2 Affine3f // update Point Associate To Map

		// numberOfCores=4
		#pragma omp parallel for num_threads(4)
		for (int i = 0; i < cornerCloudDS->size(); i++)
		{
			pcl::PointXYZI pointOri, pointSel, coeff;
			std::vector<int> pointSearchInd;
			std::vector<float> pointSearchSqDis;

			pointOri = cornerCloudDS->points[i];

			// pointAssociateToMap
			pointSel.x = transPointAssociateToMap(0,0) * pointOri.x + transPointAssociateToMap(0,1) * pointOri.y + transPointAssociateToMap(0,2) * pointOri.z + transPointAssociateToMap(0,3);
			pointSel.y = transPointAssociateToMap(1,0) * pointOri.x + transPointAssociateToMap(1,1) * pointOri.y + transPointAssociateToMap(1,2) * pointOri.z + transPointAssociateToMap(1,3);
			pointSel.z = transPointAssociateToMap(2,0) * pointOri.x + transPointAssociateToMap(2,1) * pointOri.y + transPointAssociateToMap(2,2) * pointOri.z + transPointAssociateToMap(2,3);
			pointSel.intensity = pointOri.intensity;

			kdtreeCornerFromMap->nearestKSearch(pointSel, 5, pointSearchInd, pointSearchSqDis);

			cv::Mat matA1(3, 3, CV_32F, cv::Scalar::all(0));
			cv::Mat matD1(1, 3, CV_32F, cv::Scalar::all(0));
			cv::Mat matV1(3, 3, CV_32F, cv::Scalar::all(0));

			if (pointSearchSqDis[4] < 1.0) {
				float cx = 0, cy = 0, cz = 0;
				for (int j = 0; j < 5; j++) {
					cx += local_lastcorner_transformedDS->points[pointSearchInd[j]].x;
					cy += local_lastcorner_transformedDS->points[pointSearchInd[j]].y;
					cz += local_lastcorner_transformedDS->points[pointSearchInd[j]].z;
				}
				cx /= 5; cy /= 5;  cz /= 5;

				float a11 = 0, a12 = 0, a13 = 0, a22 = 0, a23 = 0, a33 = 0;
				for (int j = 0; j < 5; j++) {
					float ax = local_lastcorner_transformedDS->points[pointSearchInd[j]].x - cx;
					float ay = local_lastcorner_transformedDS->points[pointSearchInd[j]].y - cy;
					float az = local_lastcorner_transformedDS->points[pointSearchInd[j]].z - cz;

					a11 += ax * ax; a12 += ax * ay; a13 += ax * az;
					a22 += ay * ay; a23 += ay * az;
					a33 += az * az;
				}
				a11 /= 5; a12 /= 5; a13 /= 5; a22 /= 5; a23 /= 5; a33 /= 5;

				matA1.at<float>(0, 0) = a11; matA1.at<float>(0, 1) = a12; matA1.at<float>(0, 2) = a13;
				matA1.at<float>(1, 0) = a12; matA1.at<float>(1, 1) = a22; matA1.at<float>(1, 2) = a23;
				matA1.at<float>(2, 0) = a13; matA1.at<float>(2, 1) = a23; matA1.at<float>(2, 2) = a33;

				cv::eigen(matA1, matD1, matV1);

				if (matD1.at<float>(0, 0) > 3 * matD1.at<float>(0, 1)) {

					float x0 = pointSel.x;
					float y0 = pointSel.y;
					float z0 = pointSel.z;
					float x1 = cx + 0.1 * matV1.at<float>(0, 0);
					float y1 = cy + 0.1 * matV1.at<float>(0, 1);
					float z1 = cz + 0.1 * matV1.at<float>(0, 2);
					float x2 = cx - 0.1 * matV1.at<float>(0, 0);
					float y2 = cy - 0.1 * matV1.at<float>(0, 1);
					float z2 = cz - 0.1 * matV1.at<float>(0, 2);

					float a012 = sqrt(((x0 - x1)*(y0 - y2) - (x0 - x2)*(y0 - y1)) * ((x0 - x1)*(y0 - y2) - (x0 - x2)*(y0 - y1))
									+ ((x0 - x1)*(z0 - z2) - (x0 - x2)*(z0 - z1)) * ((x0 - x1)*(z0 - z2) - (x0 - x2)*(z0 - z1))
									+ ((y0 - y1)*(z0 - z2) - (y0 - y2)*(z0 - z1)) * ((y0 - y1)*(z0 - z2) - (y0 - y2)*(z0 - z1)));

					float l12 = sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) + (z1 - z2)*(z1 - z2));

					float la = ((y1 - y2)*((x0 - x1)*(y0 - y2) - (x0 - x2)*(y0 - y1))
							  + (z1 - z2)*((x0 - x1)*(z0 - z2) - (x0 - x2)*(z0 - z1))) / a012 / l12;

					float lb = -((x1 - x2)*((x0 - x1)*(y0 - y2) - (x0 - x2)*(y0 - y1))
							   - (z1 - z2)*((y0 - y1)*(z0 - z2) - (y0 - y2)*(z0 - z1))) / a012 / l12;

					float lc = -((x1 - x2)*((x0 - x1)*(z0 - z2) - (x0 - x2)*(z0 - z1))
							   + (y1 - y2)*((y0 - y1)*(z0 - z2) - (y0 - y2)*(z0 - z1))) / a012 / l12;

					float ld2 = a012 / l12;

					float s = 1 - 0.9 * fabs(ld2);

					coeff.x = s * la;
					coeff.y = s * lb;
					coeff.z = s * lc;
					coeff.intensity = s * ld2;

					if (s > 0.1) {
						laserCloudOriCornerVec[i] = pointOri;
						coeffSelCornerVec[i] = coeff;
						laserCloudOriCornerFlag[i] = true;
					}
				}
			}
		}
	}

	void Scanmatcher_nlet::surfOptimization(void)
	{
		transPointAssociateToMap = pcl::getTransformation(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]); // trans 2 Affine3f // update Point Associate To Map

		// numberOfCores=4
		#pragma omp parallel for num_threads(4)
		for (int i = 0; i < surfaceCloudDS->size(); i++)
		{
			pcl::PointXYZI pointOri, pointSel, coeff;
			std::vector<int> pointSearchInd;
			std::vector<float> pointSearchSqDis;

			pointOri = surfaceCloudDS->points[i];

			// pointAssociateToMap
			pointSel.x = transPointAssociateToMap(0,0) * pointOri.x + transPointAssociateToMap(0,1) * pointOri.y + transPointAssociateToMap(0,2) * pointOri.z + transPointAssociateToMap(0,3);
			pointSel.y = transPointAssociateToMap(1,0) * pointOri.x + transPointAssociateToMap(1,1) * pointOri.y + transPointAssociateToMap(1,2) * pointOri.z + transPointAssociateToMap(1,3);
			pointSel.z = transPointAssociateToMap(2,0) * pointOri.x + transPointAssociateToMap(2,1) * pointOri.y + transPointAssociateToMap(2,2) * pointOri.z + transPointAssociateToMap(2,3);
			pointSel.intensity = pointOri.intensity;

			kdtreeSurfFromMap->nearestKSearch(pointSel, 5, pointSearchInd, pointSearchSqDis);

			Eigen::Matrix<float, 5, 3> matA0;
			Eigen::Matrix<float, 5, 1> matB0;
			Eigen::Vector3f matX0;

			matA0.setZero();
			matB0.fill(-1);
			matX0.setZero();

			if (pointSearchSqDis[4] < 1.0) {
				for (int j = 0; j < 5; j++) {
					matA0(j, 0) = local_lastsurf_transformedDS->points[pointSearchInd[j]].x;
					matA0(j, 1) = local_lastsurf_transformedDS->points[pointSearchInd[j]].y;
					matA0(j, 2) = local_lastsurf_transformedDS->points[pointSearchInd[j]].z;
				}

				matX0 = matA0.colPivHouseholderQr().solve(matB0);

				float pa = matX0(0, 0);
				float pb = matX0(1, 0);
				float pc = matX0(2, 0);
				float pd = 1;

				float ps = sqrt(pa * pa + pb * pb + pc * pc);
				pa /= ps; pb /= ps; pc /= ps; pd /= ps;

				bool planeValid = true;
				for (int j = 0; j < 5; j++) {
					if (fabs(pa * local_lastsurf_transformedDS->points[pointSearchInd[j]].x +
							 pb * local_lastsurf_transformedDS->points[pointSearchInd[j]].y +
							 pc * local_lastsurf_transformedDS->points[pointSearchInd[j]].z + pd) > 0.2) {
						planeValid = false;
						break;
					}
				}

				if (planeValid) {
					float pd2 = pa * pointSel.x + pb * pointSel.y + pc * pointSel.z + pd;

					float s = 1 - 0.9 * fabs(pd2) / sqrt(sqrt(pointSel.x * pointSel.x
							+ pointSel.y * pointSel.y + pointSel.z * pointSel.z));

					coeff.x = s * pa;
					coeff.y = s * pb;
					coeff.z = s * pc;
					coeff.intensity = s * pd2;

					if (s > 0.1) {
						laserCloudOriSurfVec[i] = pointOri;
						coeffSelSurfVec[i] = coeff;
						laserCloudOriSurfFlag[i] = true;
					}
				}
			}
		}
	}



	void Scanmatcher_nlet::combineOptimizationCoeffs(void)
	{
		// combine corner coeffs
		for (int i = 0; i < cornerCloudDS->size(); ++i){
			if (laserCloudOriCornerFlag[i] == true){
				laserCloudOri->push_back(laserCloudOriCornerVec[i]);
				coeffSel->push_back(coeffSelCornerVec[i]);
			}
		}
		// combine surf coeffs
		for (int i = 0; i < surfaceCloudDS->size(); ++i){
			if (laserCloudOriSurfFlag[i] == true){
				laserCloudOri->push_back(laserCloudOriSurfVec[i]);
				coeffSel->push_back(coeffSelSurfVec[i]);
			}
		}
		// reset flag for next iteration
		std::fill(laserCloudOriCornerFlag.begin(), laserCloudOriCornerFlag.end(), false);
		std::fill(laserCloudOriSurfFlag.begin(), laserCloudOriSurfFlag.end(), false);
	}

	bool Scanmatcher_nlet::LMOptimization(int iterCount)
	{
		// This optimization is from the original loam_velodyne by Ji Zhang, need to cope with coordinate transformation
		// lidar <- camera      ---     camera <- lidar
		// x = z                ---     x = y
		// y = x                ---     y = z
		// z = y                ---     z = x
		// roll = yaw           ---     roll = pitch
		// pitch = roll         ---     pitch = yaw
		// yaw = pitch          ---     yaw = roll

		// lidar -> camera
		float srx = sin(transformTobeMapped[1]);
		float crx = cos(transformTobeMapped[1]);
		float sry = sin(transformTobeMapped[2]);
		float cry = cos(transformTobeMapped[2]);
		float srz = sin(transformTobeMapped[0]);
		float crz = cos(transformTobeMapped[0]);

		int laserCloudSelNum = laserCloudOri->size();
		if (laserCloudSelNum < 50) {
			return false;
		}

		cv::Mat matA(laserCloudSelNum, 6, CV_32F, cv::Scalar::all(0));
		cv::Mat matAt(6, laserCloudSelNum, CV_32F, cv::Scalar::all(0));
		cv::Mat matAtA(6, 6, CV_32F, cv::Scalar::all(0));
		cv::Mat matB(laserCloudSelNum, 1, CV_32F, cv::Scalar::all(0));
		cv::Mat matAtB(6, 1, CV_32F, cv::Scalar::all(0));
		cv::Mat matX(6, 1, CV_32F, cv::Scalar::all(0));

		pcl::PointXYZI pointOri, coeff;

		for (int i = 0; i < laserCloudSelNum; i++) {
			// lidar -> camera
			pointOri.x = laserCloudOri->points[i].y;
			pointOri.y = laserCloudOri->points[i].z;
			pointOri.z = laserCloudOri->points[i].x;
			// lidar -> camera
			coeff.x = coeffSel->points[i].y;
			coeff.y = coeffSel->points[i].z;
			coeff.z = coeffSel->points[i].x;
			coeff.intensity = coeffSel->points[i].intensity;
			// in camera
			float arx = (crx*sry*srz*pointOri.x + crx*crz*sry*pointOri.y - srx*sry*pointOri.z) * coeff.x
					  + (-srx*srz*pointOri.x - crz*srx*pointOri.y - crx*pointOri.z) * coeff.y
					  + (crx*cry*srz*pointOri.x + crx*cry*crz*pointOri.y - cry*srx*pointOri.z) * coeff.z;

			float ary = ((cry*srx*srz - crz*sry)*pointOri.x
					  + (sry*srz + cry*crz*srx)*pointOri.y + crx*cry*pointOri.z) * coeff.x
					  + ((-cry*crz - srx*sry*srz)*pointOri.x
					  + (cry*srz - crz*srx*sry)*pointOri.y - crx*sry*pointOri.z) * coeff.z;

			float arz = ((crz*srx*sry - cry*srz)*pointOri.x + (-cry*crz-srx*sry*srz)*pointOri.y)*coeff.x
					  + (crx*crz*pointOri.x - crx*srz*pointOri.y) * coeff.y
					  + ((sry*srz + cry*crz*srx)*pointOri.x + (crz*sry-cry*srx*srz)*pointOri.y)*coeff.z;
			// lidar -> camera
			matA.at<float>(i, 0) = arz;
			matA.at<float>(i, 1) = arx;
			matA.at<float>(i, 2) = ary;
			matA.at<float>(i, 3) = coeff.z;
			matA.at<float>(i, 4) = coeff.x;
			matA.at<float>(i, 5) = coeff.y;
			matB.at<float>(i, 0) = -coeff.intensity;
		}

		cv::transpose(matA, matAt);
		matAtA = matAt * matA;
		matAtB = matAt * matB;
		cv::solve(matAtA, matAtB, matX, cv::DECOMP_QR);

		if (iterCount == 0) {

			cv::Mat matE(1, 6, CV_32F, cv::Scalar::all(0));
			cv::Mat matV(6, 6, CV_32F, cv::Scalar::all(0));
			cv::Mat matV2(6, 6, CV_32F, cv::Scalar::all(0));

			cv::eigen(matAtA, matE, matV);
			matV.copyTo(matV2);

			isDegenerate = false;
			float eignThre[6] = {100, 100, 100, 100, 100, 100};
			for (int i = 5; i >= 0; i--) {
				if (matE.at<float>(0, i) < eignThre[i]) {
					for (int j = 0; j < 6; j++) {
						matV2.at<float>(i, j) = 0;
					}
					isDegenerate = true;
				} else {
					break;
				}
			}
			matP = matV.inv() * matV2;
		}

		if (isDegenerate)
		{
			cv::Mat matX2(6, 1, CV_32F, cv::Scalar::all(0));
			matX.copyTo(matX2);
			matX = matP * matX2;
		}

		transformTobeMapped[0] += matX.at<float>(0, 0);
		transformTobeMapped[1] += matX.at<float>(1, 0);
		transformTobeMapped[2] += matX.at<float>(2, 0);
		transformTobeMapped[3] += matX.at<float>(3, 0);
		transformTobeMapped[4] += matX.at<float>(4, 0);
		transformTobeMapped[5] += matX.at<float>(5, 0);

		float deltaR = sqrt(
							pow(pcl::rad2deg(matX.at<float>(0, 0)), 2) +
							pow(pcl::rad2deg(matX.at<float>(1, 0)), 2) +
							pow(pcl::rad2deg(matX.at<float>(2, 0)), 2));
		float deltaT = sqrt(
							pow(matX.at<float>(3, 0) * 100, 2) +
							pow(matX.at<float>(4, 0) * 100, 2) +
							pow(matX.at<float>(5, 0) * 100, 2));

		if (deltaR < 0.05 && deltaT < 0.05) {
			return true; // converged
		}
		return false; // keep optimizing
	}










    Eigen::MatrixXd Scanmatcher_nlet::createScancontextfeature( pcl::PointCloud<pcl::PointXYZI> & ptcl)
    {

        const int NO_POINT = -1000;
        Eigen::MatrixXd sc_feature = NO_POINT * Eigen::MatrixXd::Ones(data->Nr, data->Ns);


        float angle, range; // wihtin 2d plane
        int ring_idx, sctor_idx;
        for (int i = 0; i < ptcl.points.size(); i++)
        {
            // xyz to polar
            range = sqrt(pow(ptcl.points[i].x,2) + pow(ptcl.points[i].y,2));
            angle = std::atan2(ptcl.points[i].y, ptcl.points[i].x) * 180/M_PI;
            if(angle < 0) angle = 360 + angle; // put azim angle in the [0, 360] domain instead of [-180, 180]


            // if range is out of roi, pass
            if(range > data->Lmax )
                continue;

            ring_idx = std::max( std::min(data->Nr, int(ceil( (range / data->Lmax) * data->Nr )) ), 1 );
            sctor_idx = std::max( std::min(data->Ns, int(ceil( (angle / 360.0) * data->Ns )) ), 1 );

            // taking maximum z
            if ( sc_feature(ring_idx-1, sctor_idx-1) < ptcl.points[i].z + 2.0) // add 2.0 this for simply directly using lidar scan in the lidar local coord (not robot base coord) / if you use robot-coord-transformed lidar scans, just set this as 0. After this, all points should be > 0
            	sc_feature(ring_idx-1, sctor_idx-1) = ptcl.points[i].z + 2.0; // update for taking maximum value at that bin
        }

        // reset no points to zero (for cosine dist later)
        for ( int row_idx = 0; row_idx < sc_feature.rows(); row_idx++ )
            for ( int col_idx = 0; col_idx < sc_feature.cols(); col_idx++ )
                if( sc_feature(row_idx, col_idx) == NO_POINT )
                	sc_feature(row_idx, col_idx) = 0;


        return sc_feature;
    }




    std::vector<float> Scanmatcher_nlet::createRingkeyfeature( Eigen::MatrixXd &sc )
    {
        std::vector<float> ringkey;
        for ( int i = 0; i < sc.rows(); i++ )
        {
            ringkey.push_back(sc.row(i).mean());
        }

        return ringkey;
    }



    PLUGINLIB_EXPORT_CLASS(nodelet_ns::Scanmatcher_nlet, nodelet::Nodelet);

}


