#include <synchronizer_nlet.h>

namespace nodelet_ns
{

    Synchronizer_nlet::Synchronizer_nlet(void)
    {
    }


    void Synchronizer_nlet::onInit(void)
    {
        ros::NodeHandle& n = getNodeHandle();

        data = Data_sglton::get();


        ////////////////// odometryHandler initialization ///////////////////
        p = gtsam::PreintegrationParams::MakeSharedU();



        p->accelerometerCovariance = data->accPSD;
		p->gyroscopeCovariance = data->gyrPSD;
        p->integrationCovariance = data->integrationPSD;


        lastmatch = Match6;


    	timer = n.createTimer(ros::Duration(0.1), &Synchronizer_nlet::synchronize2, this, true);



    	ROS_INFO("\033[1;32m----> synchronizer_nlet Started.\033[0m");


    }



    void Synchronizer_nlet::synchronize2(const ros::TimerEvent&)
    {
    	// at this point, the validity of the sm and gps data in the queues are already validated
    	// 				validity = 1) A big enough movement has been detected (validated by a threshold)
    	//						 = 2) the noise is low enough (validated by a threshold)

    	smconstraint smfront;
    	ptclconstraint ptclfront;
        nav_msgs::Odometry gpsfront;



		/////////////////////////////////
		data->initsynchronize_barrier.lock();
		/////////////////////////////////


		data->gtsam_mutex.lock();

		imuIntegratorOpt = new gtsam::PreintegratedImuMeasurements(p, gtsam::imuBias::ConstantBias()); // setting up the IMU integration for optimization
		imuIntegratorOpt->resetIntegrationAndSetBias(gtsam::imuBias::ConstantBias(data->localizerQueue4sync.front().state.segment(10,6)));


		std::cout << "x = " << data->localizerQueue4sync.front().state(0) << "y = " << data->localizerQueue4sync.front().state(1) << "z = " << data->localizerQueue4sync.front().state(2) << "vx = " << data->localizerQueue4sync.front().state(3)<< "vy = " << data->localizerQueue4sync.front().state(4)<< "vz = " << data->localizerQueue4sync.front().state(5)<< "qw = " << data->localizerQueue4sync.front().state(6)<< "qx = " << data->localizerQueue4sync.front().state(7)<< "qy = " << data->localizerQueue4sync.front().state(8)<< "qz = " << data->localizerQueue4sync.front().state(9)<< "bax = " << data->localizerQueue4sync.front().state(10)<< "bay = " << data->localizerQueue4sync.front().state(11)<< "baz = " << data->localizerQueue4sync.front().state(12)<< "bgx = " << data->localizerQueue4sync.front().state(13)<< "bgy = " << data->localizerQueue4sync.front().state(14) << "bgz = " << data->localizerQueue4sync.front().state(15) << std::endl;
		// add prior to gtsam
		gtsam::Pose3 tmp2(gtsam::Rot3::quaternion(data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9)),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
		gtsam::PriorFactor<gtsam::Pose3> prior(P(0), tmp2, gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 1e-2, 1e-2, M_PI*M_PI, 1e8, 1e8, 1e8).finished())); // TODO CHANGE FOR INITIAL POSE COVARIANCE
		data->gtSAMgraph.add(prior);
		data->initialEstimate.insert(P(0), tmp2);

		// initial velocity
		gtsam::PriorFactor<gtsam::Vector3> priorVel(V(0), gtsam::Vector3(data->localizerQueue4sync.front().state.segment(3,3)), gtsam::noiseModel::Gaussian::Covariance(data->initVelCov));
		data->gtSAMgraph.add(priorVel);
		data->initialEstimate.insert(V(0), gtsam::Vector3(data->localizerQueue4sync.front().state.segment(3,3)));

		// initial bias
		gtsam::PriorFactor<gtsam::imuBias::ConstantBias> priorBias(B(0), gtsam::imuBias::ConstantBias(data->localizerQueue4sync.front().state.segment(10,6)), gtsam::noiseModel::Gaussian::Covariance(data->initBiasCov));
		data->gtSAMgraph.add(priorBias);
		data->initialEstimate.insert(B(0), gtsam::imuBias::ConstantBias(data->localizerQueue4sync.front().state.segment(10,6)));



		data->gtsam_mutex.unlock();


		data->node_mutex.lock();

		node *tmpNode = new node;
		tmpNode->Tw_r = data->POVTranslaQuat2homoMat(data->localizerQueue4sync.front().state(0), data->localizerQueue4sync.front().state(1), data->localizerQueue4sync.front().state(2), data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9));
		tmpNode->state = data->localizerQueue4sync.front().state;
		tmpNode->cornerptcl = NULL;
		tmpNode->surfptcl = NULL;
		tmpNode->time = data->localizerQueue4sync.front().header.stamp.toSec();
		tmpNode->id = 0;
		tmpNode->has_ptcl = false;
		data->nodes.push_back(tmpNode);
		data->n_node++;

		data->node_mutex.unlock();

		data->optimize_barrier.unlock();
        data->loopcloser_barrier.unlock();
		data->sync_flag = true;

		ros::Duration(1/data->sync_rate).sleep();

    	ros::Rate rate(data->sync_rate); //1hz

    	while(ros::ok())
    	{
    		std::cout << "**********ENTER synchronizer*********" << std::endl;
    		std::cout << "size of smqueue = " << data->smconstraintQueue.size() << std::endl;
    		std::cout << "size of gpsqueue = " << data->gpsconstraintQueue.size() << std::endl;

			while(!data->smconstraintQueue.empty())
			{
				smfront = data->smconstraintQueue.front();
				data->smconstraintQueue.pop_front();

				if(smfront.id != data->n_node || smfront.previd != data->n_node_opt-1)
				{
					// outdated scan match
					continue;
				}

//				if(smfront.has_relT){
//					if(smfront.relnoise->covariance()(0) > 1e-5)
//					{
//						// too noisy
//						continue;
//					}
//				}


				if(std::sqrt(std::pow(smfront.relT.translation().x(),2)+std::pow(smfront.relT.translation().y(),2)+std::pow(smfront.relT.translation().z(),2)) > 5)
				{
					continue;
				}


				local_smconstraintQueue.push_back(smfront);
			}


			while(!data->ptclconstraintQueue.empty())
			{
				ptclfront = data->ptclconstraintQueue.front();
				data->ptclconstraintQueue.pop_front();

				if(ptclfront.id != data->n_node)
				{
					// outdated
					continue;
				}


				local_ptclconstraintQueue.push_back(ptclfront);
			}


			while(!data->gpsconstraintQueue.empty())
			{
				gpsfront = data->gpsconstraintQueue.front();
				data->gpsconstraintQueue.pop_front();


				if(data->nodes.empty())
				{
					break;
				}
				if (gpsfront.pose.covariance[0] > 2.0 || gpsfront.pose.covariance[7] > 2.0)
				{
					// GPS too noisy, skip
					continue;
				}



				local_gpsconstraintQueue.push_back(gpsfront);
			}


			std::cout << "id = " << data->n_node << std::endl;

			// With ptcl, with sm, with gps
			if(!local_smconstraintQueue.empty() && !local_gpsconstraintQueue.empty() && data->opt_flag2)
			{
				if(lastmatch == Match4 || lastmatch == Match5 || lastmatch == Match6)
				{
					std::cout << "match2" << std::endl;
					match2();
					data->opt_flag2 = false;
					lastmatch = Match2;
				}
				else
				{
					std::cout << "match1" << std::endl;
					match1();
					lastmatch = Match1;
				}
			}
			// With ptcl, without sm, with gps
			else if(!local_ptclconstraintQueue.empty() && !local_gpsconstraintQueue.empty())
			{
				std::cout << "match2" << std::endl;
				match2();
				lastmatch = Match2;
			}
			// Without ptcl, without sm, with gps
			else if(!local_gpsconstraintQueue.empty())
			{
				std::cout << "match3" << std::endl;
				match3();
				lastmatch = Match3;
			}
    		// With ptcl, with sm, without gps
			else if(!local_smconstraintQueue.empty() && local_gpsconstraintQueue.empty() && data->opt_flag2)
			{
				std::cout << "match4" << std::endl;
				match4();
				lastmatch = Match4;
			}
			// With ptcl, without sm, without gps
			else if(!local_ptclconstraintQueue.empty() && local_gpsconstraintQueue.empty())
			{
				std::cout << "match5" << std::endl;
				match5();
				lastmatch = Match5;
			}
			// Without ptcl, without sm, without gps
			else if(local_gpsconstraintQueue.empty())
			{
				std::cout << "match6" << std::endl;
				match6();
				lastmatch = Match6;
			}

			std::cout << "**********EXIT synchronizer*********" << std::endl;
			rate.sleep();
    	}



    }


    // With ptcl, with sm, with gps
    void Synchronizer_nlet::match1(void)
    {
    	double besttime = 100;
    	smconstraint smbest;
        nav_msgs::Odometry gpsbest;

		//find best match which means the closest sm and gps
		for(int i=0; i<local_gpsconstraintQueue.size(); i++)
		{
			for(int j=0; j<local_smconstraintQueue.size(); j++)
			{
				if(abs(local_gpsconstraintQueue[i].header.stamp.toSec()-local_smconstraintQueue[j].time)<besttime)
				{
					smbest = local_smconstraintQueue[j];
					gpsbest = local_gpsconstraintQueue[i];
					besttime = abs(local_gpsconstraintQueue[i].header.stamp.toSec()-local_smconstraintQueue[j].time);
					std::cout << "besttime = " << besttime << std::endl;
				}
			}
		}
		if(besttime < 0.2)
		{

			data->gtsam_mutex.lock();

			fill_imupre(smbest.time);


            // Add IMU pre-integration constraint
            const gtsam::PreintegratedImuMeasurements& preint_imu = dynamic_cast<const gtsam::PreintegratedImuMeasurements&>(*imuIntegratorOpt);
            gtsam::ImuFactor imuConstraint(P(data->n_node-1), V(data->n_node-1), P(data->n_node), V(data->n_node), B(data->n_node-1), 	preint_imu);
            data->gtSAMgraph.add(imuConstraint);
            data->initialEstimate.insert(V(data->n_node), gtsam::Vector3(data->localizerQueue4sync.front().state.segment(3,3)));
			gtsam::Pose3 tmp(gtsam::Rot3::quaternion(data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9)),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
			data->initialEstimate.insert(P(data->n_node), tmp);

            // Add bias constraint
            // The transform is a discrete brownian motion N ~ ([0 0 0 0 0 0], biasCov)
            Eigen::MatrixXd biasDisCov;
            biasDisCov = imuIntegratorOpt->deltaTij()*data->biasPSD; // 1.0/10.0 TTBM rate = lidar rate
            gtsam::noiseModel::Gaussian::shared_ptr biasCov = gtsam::noiseModel::Gaussian::Covariance(biasDisCov);
            gtsam::BetweenFactor<gtsam::imuBias::ConstantBias> biasConstraint(B(data->n_node-1), B(data->n_node), gtsam::imuBias::ConstantBias(), biasCov);
            data->gtSAMgraph.add(biasConstraint);
            data->initialEstimate.insert(B(data->n_node), gtsam::imuBias::ConstantBias((data->localizerQueue4sync.front().state.segment(10,6))));


			// Add gps constraint
			gtsam::noiseModel::Diagonal::shared_ptr gps_noise = gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(3) << gpsbest.pose.covariance[0], gpsbest.pose.covariance[7], gpsbest.pose.covariance[14]).finished());
			gtsam::GPSFactor gps_factor(P(data->n_node), gtsam::Point3(gpsbest.pose.pose.position.x, gpsbest.pose.pose.position.y, gpsbest.pose.pose.position.z), gps_noise);
			data->gtSAMgraph.add(gps_factor);


			// Add scan-matching constraint
			gtsam::BetweenFactor<gtsam::Pose3> scanMatchFactor(P(smbest.previd), P(smbest.id), smbest.relT, smbest.relnoise);
			data->gtSAMgraph.add(scanMatchFactor);
//			std::cout << "smbest.relT" << std::endl << smbest.relT << std::endl;


//			// Add compass + AHRS constraint
//			Eigen::Quaterniond q1(gpsbest.pose.pose.orientation.w, gpsbest.pose.pose.orientation.x, gpsbest.pose.pose.orientation.y, gpsbest.pose.pose.orientation.z);
//			Eigen::Quaterniond q2(data->imuQueue4imupreint.front().orientation.w, data->imuQueue4imupreint.front().orientation.x, data->imuQueue4imupreint.front().orientation.y, data->imuQueue4imupreint.front().orientation.z);
//			Eigen::Quaterniond q = q1*q2;
//			gtsam::Pose3 tmp3(gtsam::Rot3::quaternion(q.w(),q.x(),q.y(),q.z()),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
//			gtsam::PriorFactor<gtsam::Pose3> prior2(P(data->n_node), tmp3, gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 0.005236, 0.005236, 0.005236, 1000, 1000, 1000).finished()));
//			data->gtSAMgraph.add(prior2);

			// Add compass constraint
			gtsam::Pose3 tmp3(gtsam::Rot3::quaternion(gpsbest.pose.pose.orientation.w, gpsbest.pose.pose.orientation.x, gpsbest.pose.pose.orientation.y, gpsbest.pose.pose.orientation.z),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
			gtsam::PriorFactor<gtsam::Pose3> prior2(P(data->n_node), tmp3, gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 1000, 1000, 0.005, 1000, 1000, 1000).finished()));
			data->gtSAMgraph.add(prior2);

			data->gtsam_mutex.unlock();


			data->node_mutex.lock();


			node *tmpNode = new node;
			tmpNode->Tw_r = data->POVTranslaQuat2homoMat(data->localizerQueue4sync.front().state(0), data->localizerQueue4sync.front().state(1), data->localizerQueue4sync.front().state(2), data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9));
			tmpNode->state = data->localizerQueue4sync.front().state;
			tmpNode->cornerptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
			tmpNode->surfptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
			pcl::copyPointCloud(*smbest.cornerptcl,  *tmpNode->cornerptcl);// Copy this way since the point cloud vector needs a ptr
			pcl::copyPointCloud(*smbest.surfptcl,    *tmpNode->surfptcl);
			tmpNode->time = smbest.time;
			tmpNode->id = data->n_node;
			tmpNode->has_ptcl = true;
			tmpNode->sc= smbest.sc;
			tmpNode->ringkey = smbest.ringkey;
			data->nodes.push_back(tmpNode);
			data->nodes_wptcl.push_back(tmpNode);
			data->n_node++;

			data->node_mutex.unlock();

			data->optimize_barrier.unlock();
			data->loopcloser_barrier.unlock();
			data->sync_flag = true;

			local_smconstraintQueue.clear();
			local_ptclconstraintQueue.clear();
			local_gpsconstraintQueue.clear();

		}
		else
		{
			match2();
		}

    }

    // With ptcl, without sm, with gps
    void Synchronizer_nlet::match2(void)
    {
    	double besttime = 100;
    	ptclconstraint ptclbest;
        nav_msgs::Odometry gpsbest;

		//find best match which means the closest sm and gps
		for(int i=0; i<local_gpsconstraintQueue.size(); i++)
		{
			for(int j=0; j<local_ptclconstraintQueue.size(); j++)
			{
				if(abs(local_gpsconstraintQueue[i].header.stamp.toSec()-local_ptclconstraintQueue[j].time)<besttime)
				{
					ptclbest = local_ptclconstraintQueue[j];
					gpsbest = local_gpsconstraintQueue[i];
					besttime = abs(local_gpsconstraintQueue[i].header.stamp.toSec()-local_ptclconstraintQueue[j].time);
					std::cout << "besttime = " << besttime << std::endl;
				}
			}
		}
		if(besttime < 0.2)
		{

			data->gtsam_mutex.lock();

			fill_imupre(ptclbest.time);

            // Add imu factor to graph
            const gtsam::PreintegratedImuMeasurements& preint_imu = dynamic_cast<const gtsam::PreintegratedImuMeasurements&>(*imuIntegratorOpt);
            gtsam::ImuFactor imuConstraint(P(data->n_node-1), V(data->n_node-1), P(data->n_node), V(data->n_node), B(data->n_node-1), 	preint_imu);
            data->gtSAMgraph.add(imuConstraint);
            data->initialEstimate.insert(V(data->n_node), gtsam::Vector3(data->localizerQueue4sync.front().state.segment(3,3)));
			gtsam::Pose3 tmp(gtsam::Rot3::quaternion(data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9)),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
			data->initialEstimate.insert(P(data->n_node), tmp);

            // Add bias constraint
            // The transform is a discrete brownian motion N ~ ([0 0 0 0 0 0], biasCov)
            Eigen::MatrixXd biasDisCov;
            biasDisCov = imuIntegratorOpt->deltaTij()*data->biasPSD; // 1.0/10.0 TTBM rate = lidar rate
            gtsam::noiseModel::Gaussian::shared_ptr biasCov = gtsam::noiseModel::Gaussian::Covariance(biasDisCov);
            gtsam::BetweenFactor<gtsam::imuBias::ConstantBias> biasConstraint(B(data->n_node-1), B(data->n_node), gtsam::imuBias::ConstantBias(), biasCov);
            data->gtSAMgraph.add(biasConstraint);
            data->initialEstimate.insert(B(data->n_node), gtsam::imuBias::ConstantBias((data->localizerQueue4sync.front().state.segment(10,6))));


			// Add gps constraint
			gtsam::noiseModel::Diagonal::shared_ptr gps_noise = gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(3) << gpsbest.pose.covariance[0], gpsbest.pose.covariance[7], gpsbest.pose.covariance[14]).finished());
			gtsam::GPSFactor gps_factor(P(data->n_node), gtsam::Point3(gpsbest.pose.pose.position.x, gpsbest.pose.pose.position.y, gpsbest.pose.pose.position.z), gps_noise);
			data->gtSAMgraph.add(gps_factor);


//			// Add compass + AHRS constraint
//			Eigen::Quaterniond q1(gpsbest.pose.pose.orientation.w, gpsbest.pose.pose.orientation.x, gpsbest.pose.pose.orientation.y, gpsbest.pose.pose.orientation.z);
//			Eigen::Quaterniond q2(data->imuQueue4imupreint.front().orientation.w, data->imuQueue4imupreint.front().orientation.x, data->imuQueue4imupreint.front().orientation.y, data->imuQueue4imupreint.front().orientation.z);
//			Eigen::Quaterniond q = q1*q2;
//			gtsam::Pose3 tmp3(gtsam::Rot3::quaternion(q.w(),q.x(),q.y(),q.z()),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
//			gtsam::PriorFactor<gtsam::Pose3> prior2(P(data->n_node), tmp3, gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 0.005236, 0.005236, 0.005236, 1000, 1000, 1000).finished()));
//			data->gtSAMgraph.add(prior2);

			// Add compass constraint
			gtsam::Pose3 tmp3(gtsam::Rot3::quaternion(gpsbest.pose.pose.orientation.w, gpsbest.pose.pose.orientation.x, gpsbest.pose.pose.orientation.y, gpsbest.pose.pose.orientation.z),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
			gtsam::PriorFactor<gtsam::Pose3> prior2(P(data->n_node), tmp3, gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 1000, 1000, 0.005, 1000, 1000, 1000).finished()));
			data->gtSAMgraph.add(prior2);

			data->gtsam_mutex.unlock();


			data->node_mutex.lock();


			node *tmpNode = new node;
			tmpNode->Tw_r = data->POVTranslaQuat2homoMat(data->localizerQueue4sync.front().state(0), data->localizerQueue4sync.front().state(1), data->localizerQueue4sync.front().state(2), data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9));
			tmpNode->state = data->localizerQueue4sync.front().state;
			tmpNode->cornerptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
			tmpNode->surfptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
			pcl::copyPointCloud(*ptclbest.cornerptcl,  *tmpNode->cornerptcl);// Copy this way since the point cloud vector needs a ptr
			pcl::copyPointCloud(*ptclbest.surfptcl,    *tmpNode->surfptcl);
			tmpNode->time = ptclbest.time;
			tmpNode->id = data->n_node;
			tmpNode->has_ptcl = true;
			tmpNode->sc= ptclbest.sc;
			tmpNode->ringkey = ptclbest.ringkey;
			data->nodes.push_back(tmpNode);
			data->nodes_wptcl.push_back(tmpNode);
			data->n_node++;

			data->node_mutex.unlock();

			data->optimize_barrier.unlock();
			data->loopcloser_barrier.unlock();
			data->sync_flag = true;

			local_smconstraintQueue.clear();
			local_ptclconstraintQueue.clear();
			local_gpsconstraintQueue.clear();

		}
		else
		{
			match3();
		}
    }

    // Without ptcl, without sm, with gps
    void Synchronizer_nlet::match3(void)
    {
    	std::cout << "***** NO MATCH 3 ******" << std::endl;
    	data->gtsam_mutex.lock();

		fill_imupre(local_gpsconstraintQueue.back().header.stamp.toSec());


        // Add imu preintegration constraint
        const gtsam::PreintegratedImuMeasurements& preint_imu = dynamic_cast<const gtsam::PreintegratedImuMeasurements&>(*imuIntegratorOpt);
        gtsam::ImuFactor imuConstraint(P(data->n_node-1), V(data->n_node-1), P(data->n_node), V(data->n_node), B(data->n_node-1), 	preint_imu);
        data->gtSAMgraph.add(imuConstraint);
        data->initialEstimate.insert(V(data->n_node), gtsam::Vector3(data->localizerQueue4sync.front().state.segment(3,3)));
    	gtsam::Pose3 tmp(gtsam::Rot3::quaternion(data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9)),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
		data->initialEstimate.insert(P(data->n_node), tmp);

        // Add bias constraint
        // The transform is a discrete brownian motion N ~ ([0 0 0 0 0 0], biasCov)
        Eigen::MatrixXd biasDisCov;
        biasDisCov = imuIntegratorOpt->deltaTij()*data->biasPSD; // 1.0/10.0 TTBM rate = lidar rate
        gtsam::noiseModel::Gaussian::shared_ptr biasCov = gtsam::noiseModel::Gaussian::Covariance(biasDisCov);
        gtsam::BetweenFactor<gtsam::imuBias::ConstantBias> biasConstraint(B(data->n_node-1), B(data->n_node), gtsam::imuBias::ConstantBias(), biasCov);
        data->gtSAMgraph.add(biasConstraint);
        data->initialEstimate.insert(B(data->n_node), gtsam::imuBias::ConstantBias(data->localizerQueue4sync.front().state.segment(10,6)));

    	//add gps constraint
		gtsam::noiseModel::Diagonal::shared_ptr gps_noise = gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(3) << local_gpsconstraintQueue.back().pose.covariance[0], local_gpsconstraintQueue.back().pose.covariance[7], local_gpsconstraintQueue.back().pose.covariance[14]).finished());
		gtsam::GPSFactor gps_factor(P(data->n_node), gtsam::Point3(local_gpsconstraintQueue.back().pose.pose.position.x, local_gpsconstraintQueue.back().pose.pose.position.y, local_gpsconstraintQueue.back().pose.pose.position.z), gps_noise);
		data->gtSAMgraph.add(gps_factor);

//
//		// Add compass + AHRS constraint
//		Eigen::Quaterniond q1(local_gpsconstraintQueue.back().pose.pose.orientation.w, local_gpsconstraintQueue.back().pose.pose.orientation.x, local_gpsconstraintQueue.back().pose.pose.orientation.y, local_gpsconstraintQueue.back().pose.pose.orientation.z);
//		Eigen::Quaterniond q2(data->imuQueue4imupreint.front().orientation.w, data->imuQueue4imupreint.front().orientation.x, data->imuQueue4imupreint.front().orientation.y, data->imuQueue4imupreint.front().orientation.z);
//		Eigen::Quaterniond q = q1*q2;
//		gtsam::Pose3 tmp3(gtsam::Rot3::quaternion(q.w(),q.x(),q.y(),q.z()),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
//		gtsam::PriorFactor<gtsam::Pose3> prior2(P(data->n_node), tmp3, gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 0.005236, 0.005236, 0.005236, 1000, 1000, 1000).finished()));
//		data->gtSAMgraph.add(prior2);


		// Add compass constraint
		gtsam::Pose3 tmp3(gtsam::Rot3::quaternion(local_gpsconstraintQueue.back().pose.pose.orientation.w, local_gpsconstraintQueue.back().pose.pose.orientation.x, local_gpsconstraintQueue.back().pose.pose.orientation.y, local_gpsconstraintQueue.back().pose.pose.orientation.z),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
		gtsam::PriorFactor<gtsam::Pose3> prior2(P(data->n_node), tmp3, gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 1000, 1000, 0.005, 1000, 1000, 1000).finished()));
		data->gtSAMgraph.add(prior2);

    	data->gtsam_mutex.unlock();


		data->node_mutex.lock();

		node *tmpNode = new node;
		tmpNode->Tw_r = data->POVTranslaQuat2homoMat(data->localizerQueue4sync.front().state(0), data->localizerQueue4sync.front().state(1), data->localizerQueue4sync.front().state(2), data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9));
		tmpNode->state = data->localizerQueue4sync.front().state;
		tmpNode->cornerptcl = NULL;
		tmpNode->surfptcl = NULL;
		tmpNode->time = local_gpsconstraintQueue.back().header.stamp.toSec();
		tmpNode->id = data->n_node;
		tmpNode->has_ptcl = false;
		data->nodes.push_back(tmpNode);
		data->n_node++;

		data->node_mutex.unlock();

    	data->optimize_barrier.unlock();
        data->loopcloser_barrier.unlock();
		data->sync_flag = true;

		local_smconstraintQueue.clear();
		local_ptclconstraintQueue.clear();
		local_gpsconstraintQueue.clear();

    }

    // With ptcl, with sm, without gps
    void Synchronizer_nlet::match4(void)
    {
    	std::cout << "***** NO MATCH 4 ******" << std::endl;

    	data->gtsam_mutex.lock();

		fill_imupre(local_smconstraintQueue.back().time);


        // add imu preintegration constraint
        const gtsam::PreintegratedImuMeasurements& preint_imu = dynamic_cast<const gtsam::PreintegratedImuMeasurements&>(*imuIntegratorOpt);
        gtsam::ImuFactor imuConstraint(P(data->n_node-1), V(data->n_node-1), P(data->n_node), V(data->n_node), B(data->n_node-1), 	preint_imu);
        data->gtSAMgraph.add(imuConstraint);
        data->initialEstimate.insert(V(data->n_node), gtsam::Vector3(data->localizerQueue4sync.front().state.segment(3,3)));
    	gtsam::Pose3 tmp(gtsam::Rot3::quaternion(data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9)),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
		data->initialEstimate.insert(P(data->n_node), tmp);

        // Add bias constraint
        // The transform is a discrete brownian motion N ~ ([0 0 0 0 0 0], biasCov)
        Eigen::MatrixXd biasDisCov;
        biasDisCov = imuIntegratorOpt->deltaTij()*data->biasPSD; // 1.0/10.0 TTBM rate = lidar rate
        gtsam::noiseModel::Gaussian::shared_ptr biasCov = gtsam::noiseModel::Gaussian::Covariance(biasDisCov);
        gtsam::BetweenFactor<gtsam::imuBias::ConstantBias> biasConstraint(B(data->n_node-1), B(data->n_node), gtsam::imuBias::ConstantBias(), biasCov);
        data->gtSAMgraph.add(biasConstraint);
        data->initialEstimate.insert(B(data->n_node), gtsam::imuBias::ConstantBias(data->localizerQueue4sync.front().state.segment(10,6)));

    	//Add scan-matching constraint
    	gtsam::BetweenFactor<gtsam::Pose3> scanMatchFactor(P(local_smconstraintQueue.back().previd), P(local_smconstraintQueue.back().id), local_smconstraintQueue.back().relT, local_smconstraintQueue.back().relnoise);
    	data->gtSAMgraph.add(scanMatchFactor);

    	std::cout << "previd = " << local_smconstraintQueue.back().previd << std::endl;
    	std::cout << "id = " << local_smconstraintQueue.back().id << std::endl;


//		// Add AHRS constraint
//		gtsam::Pose3 tmp3(gtsam::Rot3::quaternion(data->imuQueue4imupreint.front().orientation.w, data->imuQueue4imupreint.front().orientation.x, data->imuQueue4imupreint.front().orientation.y, data->imuQueue4imupreint.front().orientation.z),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
//		gtsam::PriorFactor<gtsam::Pose3> prior2(P(data->n_node), tmp3, gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 0.005236, 0.005236, 1000, 1000, 1000, 1000).finished()));
//		data->gtSAMgraph.add(prior2);

    	data->gtsam_mutex.unlock();


		data->node_mutex.lock();


		node *tmpNode = new node;
		tmpNode->Tw_r = data->POVTranslaQuat2homoMat(data->localizerQueue4sync.front().state(0), data->localizerQueue4sync.front().state(1), data->localizerQueue4sync.front().state(2), data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9));
		tmpNode->state = data->localizerQueue4sync.front().state;
		tmpNode->cornerptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
		tmpNode->surfptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
		pcl::copyPointCloud(*local_smconstraintQueue.back().cornerptcl,  *tmpNode->cornerptcl);// Copy this way since the point cloud vector needs a ptr
		pcl::copyPointCloud(*local_smconstraintQueue.back().surfptcl,    *tmpNode->surfptcl);
		tmpNode->time = local_smconstraintQueue.back().time;
		tmpNode->id = data->n_node;
		tmpNode->has_ptcl = true;
		tmpNode->sc= local_smconstraintQueue.back().sc;
		tmpNode->ringkey = local_smconstraintQueue.back().ringkey;
		data->nodes.push_back(tmpNode);
		data->nodes_wptcl.push_back(tmpNode);
		data->n_node++;

		data->node_mutex.unlock();

    	data->optimize_barrier.unlock();
        data->loopcloser_barrier.unlock();
		data->sync_flag = true;

		local_smconstraintQueue.clear();
		local_ptclconstraintQueue.clear();
		local_gpsconstraintQueue.clear();
    }

    // With ptcl, without sm, without gps
    void Synchronizer_nlet::match5(void)
    {
    	std::cout << "***** NO MATCH 5 ******" << std::endl;

    	data->gtsam_mutex.lock();

		fill_imupre(local_ptclconstraintQueue.back().time);



        // add imu preintegration constraint
        const gtsam::PreintegratedImuMeasurements& preint_imu = dynamic_cast<const gtsam::PreintegratedImuMeasurements&>(*imuIntegratorOpt);
        gtsam::ImuFactor imuConstraint(P(data->n_node-1), V(data->n_node-1), P(data->n_node), V(data->n_node), B(data->n_node-1), 	preint_imu);
        data->gtSAMgraph.add(imuConstraint);
        data->initialEstimate.insert(V(data->n_node), gtsam::Vector3(data->localizerQueue4sync.front().state.segment(3,3)));
    	gtsam::Pose3 tmp(gtsam::Rot3::quaternion(data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9)),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
		data->initialEstimate.insert(P(data->n_node), tmp);

        // Add bias constraint
        // The transform is a discrete brownian motion N ~ ([0 0 0 0 0 0], biasCov)
        Eigen::MatrixXd biasDisCov;
        biasDisCov = imuIntegratorOpt->deltaTij()*data->biasPSD; // 1.0/10.0 TTBM rate = lidar rate
        gtsam::noiseModel::Gaussian::shared_ptr biasCov = gtsam::noiseModel::Gaussian::Covariance(biasDisCov);
        gtsam::BetweenFactor<gtsam::imuBias::ConstantBias> biasConstraint(B(data->n_node-1), B(data->n_node), gtsam::imuBias::ConstantBias(), biasCov);
        data->gtSAMgraph.add(biasConstraint);
        data->initialEstimate.insert(B(data->n_node), gtsam::imuBias::ConstantBias(data->localizerQueue4sync.front().state.segment(10,6)));


//		// Add AHRS constraint
//		gtsam::Pose3 tmp3(gtsam::Rot3::quaternion(data->imuQueue4imupreint.front().orientation.w, data->imuQueue4imupreint.front().orientation.x, data->imuQueue4imupreint.front().orientation.y, data->imuQueue4imupreint.front().orientation.z),gtsam::Point3(data->localizerQueue4sync.front().state(0),data->localizerQueue4sync.front().state(1),data->localizerQueue4sync.front().state(2)));
//		gtsam::PriorFactor<gtsam::Pose3> prior2(P(data->n_node), tmp3, gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 0.005236, 0.005236, 1000, 1000, 1000, 1000).finished()));
//		data->gtSAMgraph.add(prior2);

    	data->gtsam_mutex.unlock();


		data->node_mutex.lock();


		node *tmpNode = new node;
		tmpNode->Tw_r = data->POVTranslaQuat2homoMat(data->localizerQueue4sync.front().state(0), data->localizerQueue4sync.front().state(1), data->localizerQueue4sync.front().state(2), data->localizerQueue4sync.front().state(6), data->localizerQueue4sync.front().state(7), data->localizerQueue4sync.front().state(8), data->localizerQueue4sync.front().state(9));
		tmpNode->state = data->localizerQueue4sync.front().state;
		tmpNode->cornerptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
		tmpNode->surfptcl.reset(new pcl::PointCloud<pcl::PointXYZI>());
		pcl::copyPointCloud(*local_ptclconstraintQueue.back().cornerptcl,  *tmpNode->cornerptcl);// Copy this way since the point cloud vector needs a ptr
		pcl::copyPointCloud(*local_ptclconstraintQueue.back().surfptcl,    *tmpNode->surfptcl);
		tmpNode->time = local_ptclconstraintQueue.back().time;
		tmpNode->id = data->n_node;
		tmpNode->has_ptcl = true;
		tmpNode->sc= local_ptclconstraintQueue.back().sc;
		tmpNode->ringkey = local_ptclconstraintQueue.back().ringkey;
		data->nodes.push_back(tmpNode);
		data->nodes_wptcl.push_back(tmpNode);
		data->n_node++;

		data->node_mutex.unlock();

    	data->optimize_barrier.unlock();
        data->loopcloser_barrier.unlock();
		data->sync_flag = true;

		local_smconstraintQueue.clear();
		local_ptclconstraintQueue.clear();
		local_gpsconstraintQueue.clear();
    }

    // Without ptcl, without sm, without gps
    void Synchronizer_nlet::match6(void)
    {
    	std::cout << "***** NO MATCH 6 ******" << std::endl;
		local_smconstraintQueue.clear();
		local_ptclconstraintQueue.clear();
		local_gpsconstraintQueue.clear();

    }



    void Synchronizer_nlet::fill_imupre(double time_limit)
    {
    	while (!data->localizerQueue4sync.empty())
    	{
    		if(data->localizerQueue4sync.front().header.seq == data->imuQueue4imupreint.front().header.seq)
    		{
    			imuIntegratorOpt->resetIntegrationAndSetBias(gtsam::imuBias::ConstantBias(data->localizerQueue4sync.front().state.segment(10,6)));
    			break;
    		}
    		else
    			data->localizerQueue4sync.pop_front();
    	}
        while (!data->imuQueue4imupreint.empty())
        {
            // pop and integrate imu data that is between two optimizations
            if (data->imuQueue4imupreint.front().header.stamp.toSec() < time_limit)
            {
                imuIntegratorOpt->integrateMeasurement(gtsam::Vector3(data->imuQueue4imupreint.front().linear_acceleration.x,
                													  data->imuQueue4imupreint.front().linear_acceleration.y,
																	  data->imuQueue4imupreint.front().linear_acceleration.z),
                        							   gtsam::Vector3(data->imuQueue4imupreint.front().angular_velocity.x,
                        									   	   	  data->imuQueue4imupreint.front().angular_velocity.y,
																	  data->imuQueue4imupreint.front().angular_velocity.z), 1.0/data->imu_rate);
                data->imuQueue4imupreint.pop_front();
                data->localizerQueue4sync.pop_front();
            }
            else
                break;
        }
    }






    PLUGINLIB_EXPORT_CLASS(nodelet_ns::Synchronizer_nlet, nodelet::Nodelet);

}
