#include <optimizer_nlet.h>

namespace nodelet_ns
{

    Optimizer_nlet::Optimizer_nlet(void)
    {
    }

    void Optimizer_nlet::onInit()
    {
        ros::NodeHandle& n = getNodeHandle();

        data = Data_sglton::get();


        timer = n.createTimer(ros::Duration(0.1), &Optimizer_nlet::optimize_graph, this, true);

        pubPath = n.advertise<nav_msgs::Path>("lio_sam/mapping/path", 1);
        pubLaserCloudSurround = n.advertise<sensor_msgs::PointCloud2>("lio_sam/mapping/map_global", 1);
        pubOptDone = n.advertise<std_msgs::Float32>("optimization_done", 1);



    	ROS_INFO("\033[1;32m----> optimizer_nlet Started.\033[0m");


    }

    void Optimizer_nlet::optimize_graph(const ros::TimerEvent&)
    {
    	bool flag = false;
    	while(ros::ok())
    	{

			////////////////////////////////
			data->optimize_barrier.lock();
			////////////////////////////////
    		std::cout << "==========ENTER optimizer=========" << std::endl;


			// update iSAM
			data->gtsam_mutex.lock();

			gtsam::NonlinearFactorGraph graphtmp = data->gtSAMgraph;
			gtsam::Values initialtmp = data->initialEstimate;
			data->gtSAMgraph.resize(0);
			data->initialEstimate.clear();

			data->gtsam_mutex.unlock();

//			ros::Duration(2).sleep();
			data->isam->update(graphtmp, initialtmp);
			data->isam->update();
			data->isam->update();
			data->isam->update();
			data->isam->update();
			data->isam->update();
			data->isam->update();


//				data->n_node_opt = data->isam->calculateEstimate().size();
			data->n_node_opt = data->isam->calculateEstimate().size()/3; // 3 because there is 3 types of nodes (P,V,B)
//				data->poseCovariance << data->isam->marginalCovariance(P(data->n_node_opt-1));
			Eigen::MatrixXd poseCov = data->isam->marginalCovariance(P(data->n_node_opt-1));
			data->lastCovariance << poseCov.block<3,3>(0,0), Eigen::MatrixXd::Zero(3,3), poseCov.block<3,3>(0,3), Eigen::MatrixXd::Zero(3,6),
									Eigen::MatrixXd::Zero(3,3), data->isam->marginalCovariance(V(data->n_node_opt-1)), Eigen::MatrixXd::Zero(3,9),
									poseCov.block<3,3>(3,0), Eigen::MatrixXd::Zero(3,3), poseCov.block<3,3>(3,3), Eigen::MatrixXd::Zero(3,6),
									Eigen::MatrixXd::Zero(6,9), data->isam->marginalCovariance(B(data->n_node_opt-1));
			data->graph_opt = data->isam->calculateEstimate();


			data->globalPath.poses.clear();

			data->node_mutex.lock();

			for (int i = 0; i < data->n_node_opt; ++i)
			{

				data->nodes[i]->Tw_r = data->graph_opt.at<gtsam::Pose3>(P(i)).matrix();


				data->nodes[i]->state.segment(0,3) = data->graph_opt.at<gtsam::Pose3>(P(i)).translation();
				data->nodes[i]->state.segment(3,3) = data->graph_opt.at<gtsam::Vector3>(V(i));
				// CAREFUL HERE, ERROR IN GTSAM, quaternion x is in fact w
				data->nodes[i]->state(6) = data->graph_opt.at<gtsam::Pose3>(P(i)).rotation().quaternion().x();
				data->nodes[i]->state(7) = data->graph_opt.at<gtsam::Pose3>(P(i)).rotation().quaternion().y();
				data->nodes[i]->state(8) = data->graph_opt.at<gtsam::Pose3>(P(i)).rotation().quaternion().z();
				data->nodes[i]->state(9) = data->graph_opt.at<gtsam::Pose3>(P(i)).rotation().quaternion().w();
				data->nodes[i]->state.segment(10,6) = data->graph_opt.at<gtsam::imuBias::ConstantBias>(B(i)).vector();


				geometry_msgs::PoseStamped pose_stamped;
				pose_stamped.header.stamp = ros::Time().fromSec(data->nodes[i]->time);
				pose_stamped.header.frame_id = "odom";
				pose_stamped.pose.position.x = data->graph_opt.at<gtsam::Pose3>(P(i)).translation().x();
				pose_stamped.pose.position.y = data->graph_opt.at<gtsam::Pose3>(P(i)).translation().y();
				pose_stamped.pose.position.z = data->graph_opt.at<gtsam::Pose3>(P(i)).translation().z();
				// CAREFUL HERE, ERROR IN GTSAM, quaternion x is in fact w
				pose_stamped.pose.orientation.w = data->graph_opt.at<gtsam::Pose3>(P(i)).rotation().quaternion().x();
				pose_stamped.pose.orientation.x = data->graph_opt.at<gtsam::Pose3>(P(i)).rotation().quaternion().y();
				pose_stamped.pose.orientation.y = data->graph_opt.at<gtsam::Pose3>(P(i)).rotation().quaternion().z();;
				pose_stamped.pose.orientation.z = data->graph_opt.at<gtsam::Pose3>(P(i)).rotation().quaternion().w();

				data->globalPath.poses.push_back(pose_stamped);
			}


			data->node_mutex.unlock();



			//// PUBLISH Optimization done
			std_msgs::Float32 dummy;
			dummy.data = 0;
			pubOptDone.publish(dummy);



			//// PUBLISH PATH
			data->globalPath.header.stamp = ros::Time::now();
			data->globalPath.header.frame_id = "odom";
			pubPath.publish(data->globalPath);



			//// PUBLISH GLOBAL MAP
			data->localMapCorner->clear();
			data->localMapSurf->clear();
			data->localMapNodeVec.clear();
			std::deque<node*> tmpvec;
			double last_time = 0;
			pcl::PointCloud<pcl::PointXYZI>::Ptr globalMapKeyFrames(new pcl::PointCloud<pcl::PointXYZI>());
			pcl::PointCloud<pcl::PointXYZI>::Ptr globalMapKeyFramesDS(new pcl::PointCloud<pcl::PointXYZI>());

			pcl::PointCloud<pcl::PointXYZI>::Ptr tmp1(new pcl::PointCloud<pcl::PointXYZI>());
			pcl::PointCloud<pcl::PointXYZI>::Ptr tmp2(new pcl::PointCloud<pcl::PointXYZI>());
			// extract visualized and downsampled key frames
			for (int i = 0; i < data->n_node_opt; i++)
			{
				if(data->nodes[i]->has_ptcl == true)
				{
					pcl::transformPointCloud (*data->nodes[i]->cornerptcl, *tmp1, data->nodes[i]->Tw_r);
					*globalMapKeyFrames += *tmp1;

					pcl::transformPointCloud (*data->nodes[i]->surfptcl, *tmp2, data->nodes[i]->Tw_r);
					*globalMapKeyFrames += *tmp2;

					if(data->n_node_opt-i <= data->localMapN)
					{
						tmpvec.push_front(data->nodes[i]);
					}
				}
			}
			// obtain a consistent local map
			if(!tmpvec.empty())
			{
				for (int i=0; i<tmpvec.size(); i++)
				{
					if(last_time - tmpvec[i]->time > 8)
					{
						break;
					}
					else
					{
						data->localMapNodeVec.push_front(*tmpvec[i]);
						last_time = tmpvec[i]->time;
						*data->localMapCorner += *tmp1;
						*data->localMapSurf += *tmp2;
					}
				}
			}


			data->opt_flag = true;
			if(flag == true)
			{
				data->opt_flag2 = true;
				flag = false;
			}
			if(data->opt_flag2 == false)
			{
				flag = true;
			}

			// downsample visualized points
			pcl::VoxelGrid<pcl::PointXYZI> downSizeFilterGlobalMapKeyFrames; // for global map visualization
//			pcl::RandomSample<pcl::PointXYZI> test;
//			downSizeFilterGlobalMapKeyFrames.setLeafSize(0.5, 0.5, 0.5); // globalMapVisualizationLeafSize=1.0, for global map visualization
//			downSizeFilterGlobalMapKeyFrames.setInputCloud(globalMapKeyFrames);
//			downSizeFilterGlobalMapKeyFrames.filter(*globalMapKeyFramesDS);

			sensor_msgs::PointCloud2 tempCloud;
//			pcl::toROSMsg(*globalMapKeyFramesDS, tempCloud);
			pcl::toROSMsg(*globalMapKeyFrames, tempCloud);
			tempCloud.header.stamp = ros::Time::now();
			tempCloud.header.frame_id = "odom";
			pubLaserCloudSurround.publish(tempCloud);

    		std::cout << "==========EXIT optimizer=========" << std::endl;
    	}
    }




    PLUGINLIB_EXPORT_CLASS(nodelet_ns::Optimizer_nlet, nodelet::Nodelet);
}
