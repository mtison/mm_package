#include <gps_nlet.h>

namespace nodelet_ns
{
    Gps_nlet::Gps_nlet(void)
    {
    }

    void Gps_nlet::onInit(void)
    {
        ros::NodeHandle& n = getNodeHandle();

        data = Data_sglton::get();

        subGPSfix   = n.subscribe<sensor_msgs::NavSatFix> ("gps/fixz", 200, &Gps_nlet::gpsfixHandler, this, ros::TransportHints().tcpNoDelay());
        pubGPS = n.advertise<nav_msgs::Odometry> ("gps_local", 200);


        Tw_utm_definedflag = false;

        std::string tmp;
        std::ifstream theFile("example.csv");
        for(int i=0; i<2757; i++)
        {
        	getline(theFile, tmp);
        	headingvec.push_back(std::stod(tmp));
        }

    	ROS_INFO("\033[1;32m----> gps_nlet Started.\033[0m");

    }



    // https://www.mathworks.com/matlabcentral/fileexchange/45699-ll2utm-and-utm2ll?s_tid=mwa_osa_a
    void Gps_nlet::gpsfixHandler(const sensor_msgs::NavSatFix::ConstPtr& msg)
    {

    	sensor_msgs::NavSatFix gps_raw = *msg;




    	// ADD ONLY FOR PROVECTUS DATASET
    	double heading = gps_raw.position_covariance[8];
    	gps_raw.position_covariance[8] = 0.1; // constant high altitude variance

//    	// ADD ONLY FOR MIT DATASET
//    	double heading = headingvec[gps_raw.header.seq-25988];


    	/////////// Transform to UTM frame //////////////
    	double lat_deg = gps_raw.latitude;
    	double lon_deg = gps_raw.longitude;

    	double lat_rad = lat_deg*M_PI/180;
    	double lon_rad = lon_deg*M_PI/180;

    	// Using wgs84 datum
    	double A1 = 6378137.0;
    	double F1 = 298.257223563;


    	// constants
    	double D0 = 180/M_PI;	// conversion rad to deg
    	double K0 = 0.9996;		// UTM scale factor
    	double X0 = 500000;		// UTM false East (m)




    	double F0 = round((lon_deg + 183)/6);


    	double B1 = A1*(1 - 1/F1);
    	double E1 = sqrt((A1*A1 - B1*B1)/(A1*A1));
    	double P0 = 0/D0;
    	double L0 = (6*F0 - 183)/D0;	// UTM origin longitude (rad)
    	double Y0 = 1e7*(lat_rad < 0);		// UTM false northern (m)
    	double N = K0*A1;



    	Eigen::VectorXd C;
    	C = coef(E1,0);
    	double  B = C(0)*P0 + C(1)*sin(2*P0) + C(2)*sin(4*P0) + C(3)*sin(6*P0) + C(4)*sin(8*P0);
    	double YS = Y0 - N*B;

    	C = coef(E1,2);
    	double L = log(   tan(M_PI/4 + lat_rad/2)*     pow((1 - E1*sin(lat_rad))/(1 + E1*sin(lat_rad)), E1/2)   );
    	std::complex<double> z;
    	z = {atan(sinh(L)/cos(lon_rad - L0)), log(tan(M_PI/4 + asin(sin(lon_rad - L0)/cosh(L))/2))};
    	std::complex<double> Z = N*C(0)*z + N*(C(1)*sin(2.0*z) + C(2)*sin(4.0*z) + C(3)*sin(6.0*z) + C(4)*sin(8.0*z));
    	double x = Z.imag() + X0;
    	double y = Z.real() + YS;
    	T_utm_r = data->POVtranslaEul2HomoMat(x,y,gps_raw.altitude,0,0,heading);

    	int zone = F0*copysign(1.0, lat_deg); // this is the utm zone

    	//////////////// LOCAL MAP AND INERTIAL MAP FIXING /////////////////
    	if(Tw_utm_definedflag != true)
    	{
			Eigen::VectorXd last;
			last = Eigen::VectorXd::Zero(16);
			Eigen::Matrix4d T_w_r_last;



			for(int i=0; i<data->localizerQueue4sync.size(); i++)
			{
				last = data->localizerQueue4sync[i].state;
				if(data->localizerQueue4sync[i].header.stamp.toSec() >= gps_raw.header.stamp.toSec())
				{
					break;
				}
			}
			T_w_r_last = data->POVTranslaQuat2homoMat(last(0), last(1), last(2), last(6), last(7), last(8), last(9));

			T_utm_w = T_utm_r * T_w_r_last.inverse();


    		Tw_utm_definedflag = true;
    	}


    	T_w_r = T_utm_w.inverse()*T_utm_r;


        nav_msgs::Odometry P;
    	data->POVhomoMat2TranslaQuat(T_w_r, &P.pose.pose.position.x, &P.pose.pose.position.y, &P.pose.pose.position.z,  &P.pose.pose.orientation.w, &P.pose.pose.orientation.x, &P.pose.pose.orientation.y, &P.pose.pose.orientation.z);
        P.header.stamp = gps_raw.header.stamp;
        P.header.frame_id = "odom";
        P.pose.covariance[0] = gps_raw.position_covariance[0];
        P.pose.covariance[7] = gps_raw.position_covariance[4];
        P.pose.covariance[14] = gps_raw.position_covariance[8];

        pubGPS.publish(P);




    }

    Eigen::VectorXd Gps_nlet::coef(double e, int m)
    {
    	/*COEF Projection coefficients
    	*	COEF(E,M) returns a vector of 5 coefficients from:
    	*		E = first ellipsoid excentricity
    	*		M = 0 for transverse mercator
    	*		M = 1 for transverse mercator reverse coefficients
    	*		M = 2 for merdian arc
		*/
//    	Eigen::MatrixXd c0(5,9);
    	Eigen::VectorXd c(5,1);
    	c = Eigen::VectorXd::Zero(5);

    	switch(m)
    	{
    		case 0:
    			c(0) = (-175.0/16384.0)*pow(e,8)+(-5.0/256.0)*pow(e,6)+(-3.0/64.0)*pow(e,4)+(-1.0/4.0)*pow(e,2)+1.0;
    			c(1) = (-105.0/4096.0)*pow(e,8)+(-45.0/1024.0)*pow(e,6)+(-3.0/32.0)*pow(e,4)+(-3.0/8.0)*pow(e,2);
    			c(2) = (525.0/16384.0)*pow(e,8)+(45.0/1024.0)*pow(e,6)+(15.0/256.0)*pow(e,4);
    			c(3) = (-175.0/12288.0)*pow(e,8)+(-35.0/3072.0)*pow(e,6);
    			c(4) = (315.0/131072.0)*pow(e,8);
    			break;

    		case 1:
    			c(0) = (-175.0/16384.0)*pow(e,8)+(-5.0/256.0)*pow(e,6)+(-3.0/64.0)*pow(e,4)+(-1.0/4.0)*pow(e,2)+1.0;
    			c(1) = (1.0/61440.0)*pow(e,8)+(7.0/2048.0)*pow(e,6)+(1.0/48.0)*pow(e,4)+(1.0/8.0)*pow(e,2);
    			c(2) = (559.0/368640.0)*pow(e,8)+(3.0/1280.0)*pow(e,6)+(1.0/768.0)*pow(e,4);
    			c(3) = (283.0/430080.0)*pow(e,8)+(17.0/30720.0)*pow(e,6);
    			c(4) = (4397.0/41287680.0)*pow(e,8);
    			break;

    		case 2:
    			c(0) = (-175.0/16384.0)*pow(e,8)+(-5.0/256.0)*pow(e,6)+(-3.0/64.0)*pow(e,4)+(-1.0/4.0)*pow(e,2)+1.0;
    			c(1) = (-901.0/184320.0)*pow(e,8)+(-9.0/1024.0)*pow(e,6)+(-1.0/96.0)*pow(e,4)+(1.0/8.0)*pow(e,2);
    			c(2) = (-311.0/737280.0)*pow(e,8)+(17.0/5120.0)*pow(e,6)+(13.0/768.0)*pow(e,4);
    			c(3) = (899.0/430080.0)*pow(e,8)+(61.0/15360.0)*pow(e,6);
    			c(4) = (49561.0/41287680.0)*pow(e,8);
    			break;

    	}


    	return c;

    }



    PLUGINLIB_EXPORT_CLASS(nodelet_ns::Gps_nlet, nodelet::Nodelet);
}
